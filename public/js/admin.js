$('.demo.sidebar')
  .sidebar('setting', 'transition', 'overlay')
  .sidebar('toggle')
;

// using context
$('.ui.sidebar')
  .sidebar({
    context: $('.bottom.segment')
  })
  .sidebar('attach events', '.menu .item.side')
;

$('.ui.dropdown')
  .dropdown()
;

$('.ui.dropdown.enlace')
  .dropdown({
  	on: 'hover'
  })
;

$('.ui.dropdown.log')
  .dropdown({
  	on: 'hover'
  })
;