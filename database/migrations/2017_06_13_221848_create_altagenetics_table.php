<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAltageneticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('altagenetics', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('seccion');
            $table->string('categoria');
            $table->string('imagen');
            $table->text('descripcion');
            $table->string('precio');
            $table->string('codigo');
            $table->string('url');
            $table->boolean('destacado')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('altagenetics');
    }
}
