<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDescuentoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('descuento', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('imagen');
			$table->text('video');
            $table->text('descripcion');
            $table->text('restriccion');
            $table->text('empresa');
            $table->text('direccion');
            $table->text('ciudad');
            $table->text('telefono');
            $table->string('categoria');
            $table->string('descuento');
            $table->string('red_twitter');
            $table->string('red_facebook');
            $table->string('red_instagram');
            $table->string('empresa_email');
            $table->text('coordenadas');
            $table->string('carnet_imagen');
            $table->string('carnet_codigo');
            $table->string('fecha_vencimiento');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
