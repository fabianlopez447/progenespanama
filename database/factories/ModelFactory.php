<?php
use \App\Descuento;
use Faker\Generator;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(Descuento::class, function(Generator $faker){
	$array = [
		'nombre' => $faker->title,
		'imagen' => 'image_1481054314.jpg',
		'descripcion' => $faker->realText,
		'restriccion' => $faker->realText,
		'empresa' => $faker->company,
		'direccion' => $faker->streetAddress,
		'ciudad' => $faker->city,
		'telefono' => $faker->phoneNumber,
		'categoria' => '1',
		'descuento' => '50',
		'empresa_email' => $faker->companyEmail,
		'coordenadas' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15792.267080313137!2d-62.74068838232065!3d8.296154387753937!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8dcbfbd60ce82747%3A0x744c6352b0c97c30!2sAlta+Vista%2C+Ciudad+Guayana+8050%2C+Bol%C3%ADvar!5e0!3m2!1ses!2sve!4v1484429690925" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
		'carnet_imagen' => 'image_1481054314.jpg',
		'carnet_codigo' => $faker->numberBetween
	];
	return $array;
});
