<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class descuentoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Descuento::class, 20)->create();
    }
}
