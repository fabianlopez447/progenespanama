<html>
<head>
	<title></title>
	<link rel="stylesheet" href="http://superdescuentospanama.com/files/semantic.css">
</head>
<body>
<?php $desc = \App\Descuento::orderBy('created_at','desc')->get();
$sem = \App\descuento_semanal::orderBy('created_at','desc')->paginate(1); ?>
	<div style="margin:30px; background:#F2F2F2;border-radius:5px;padding:30px;float:right;">
		<div>
		@foreach($sem as $Semanal)
			@foreach($desc as $Descuento)
				@if($Semanal->id_descuento == $Descuento->id)
					<div style="width:100%;">
						<img src="http://superdescuentospanama.com/public/descuento/{!!$Descuento->imagen!!}" style="box-shadow: 0px 0px 3px black; width:100%;" alt="">
					</div>
					<div style="width:100%;">
						<h1>{!!$Descuento->nombre!!}</h1>
						<p>{!!date("d-m-Y", strtotime($Descuento->created_at))!!}</p>
					</div>
					<div style="width:100%;">
						<div style="text-align:justify;">
							<P>{!!$Descuento->descripcion!!}</P>
							<p>El descuento se vence el {!!$Descuento->fecha_vencimiento!!}</p>
						</div>
						<div>
							<a href="http://superdescuentospanama.com/descuento/{!!$Descuento->id!!}/detalles" style="text-decoration: none; color: white;padding: 5px; font-size: 24px; background: #58ACFA; border-bottom: 1px solid #A9D0F5; border-radius: 5px;" onMouseOver="this.style.cssText='background: #2E9AFE'" >Ver descuento</a>
						</div>
					</div><br>
					<hr><br>
				@endif
			@endforeach
		@endforeach
		</div>
	</div>
</body>
</html>