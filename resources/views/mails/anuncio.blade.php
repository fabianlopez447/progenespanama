<html>
<head>
	<title></title>
	<link rel="stylesheet" href="http://superdescuentospanama.com/files/semantic.css">
</head>
<body>
	<div style="margin:30px; background:#F2F2F2;border-radius:5px;padding:30px;float:right;">
		<div>
			<div style="float:left; width:40%;">
				<img src="http://www.julietakohan.com/wp-content/uploads/2016/06/20desc.jpg" style="box-shadow: 0px 0px 3px black; width:100%;" alt="">
			</div>
			<div style="float:right; width:58%;">
				<h1>{!!$nombre!!}</h1>
				<p>{!!date("d-m-Y")!!}</p>
			</div>
			<div style="float:right; width:58%;">
				<div style="text-align:justify;">
					<P>{!!$descripcion!!}</P>
					<p>El descuento se vence el {!!$fecha_vencimiento!!}</p>
				</div>
				<div>
					<a href="http://superdescuentospanama.com" style="text-decoration: none; color: white;padding: 5px; font-size: 24px; background: #58ACFA; border-bottom: 1px solid #A9D0F5; border-radius: 5px;" onMouseOver="this.style.cssText='background: #2E9AFE'" >Ver descuento</a>
				</div>
			</div>
		</div>
	</div>
</body>
</html>