@extends('layouts.template')
@section('titulo', 'Activación de carnet')
@section('contenido')
	<div class="ui container">
		<div class="ui grid">
			<div class="sixteen wide column">
				<h1 class="header center aligned">Activar Carnet</h1>
			</div>
			@foreach($desc as $Descuento)
			  @if(date('d/m/Y', strtotime($Descuento->fecha_vencimiento)) > date('d/m/Y'))
				<div class="seven wide column">
					<img src="{!!asset('descuentos/'.$Descuento->imagen)!!}" width="100%" alt="" class="imagen">
				</div>
				<div class="nine wide column">
					<h3>{!!$Descuento->nombre!!}</h3>
					<sub>
						@if($Descuento->categoria == 1)
							<i  class="icon diamond"></i>Articulos Exclusivos
						@endif
						@if($Descuento->categoria == 2)
							<i class="icon eye"></i>Belleza y Estetica
						@endif
						@if($Descuento->categoria == 3)
							<i class="icon puzzle"></i>Entretenimiento
						@endif
						@if($Descuento->categoria == 4)
							<i class="icon hotel"></i>Hoteles y Turismo
						@endif
						@if($Descuento->categoria == 5)
							<i class="icon paw"></i>Mascotas
						@endif
						@if($Descuento->categoria == 6)
							<i class="icon child"></i>Niños y Bebes
						@endif
						@if($Descuento->categoria == 7)
							<i class="icon food"></i>Restaurantes y Cafeterías
						@endif
						@if($Descuento->categoria == 8)
							<i class="icon heartbeat"></i>Salud
						@endif
						@if($Descuento->categoria == 9)
							<i class="icon car"></i>Servicios Automotrices
						@endif
						@if($Descuento->categoria == 10)
							<i class="icon building outline"></i>Servicios Corporativos
						@endif
						@if($Descuento->categoria == 11)
							<i class="icon shopping bag"></i>Tiendas
						@endif
					</sub>
					<p>{!!$Descuento->descripcion!!}</p>
				</div>
				@endif
			@endforeach
		</div>
			<br>
	@if(count($errors)> 0)
<div class="ui inverted red segment">
  <ul>ERROR<hr>
  	 @foreach($errors->all() as $error)
  	 			
  	 			<li>{{$error}}</li>
  	 @endforeach
  </ul>
</div>
@endif
		<div class="ui segment">
			{!!Form::open(['route'=>'admin.suscriptor.store', 'method'=>'POST', 'class'=>'ui form'])!!}
				<div class="field">
				    <label>Nombres</label>
				    {!!Form::text('nombre', null)!!}
				</div>
				<div class="field">
				    <label>Apellidos</label>
				    {!!Form::text('apellido', null)!!}
				</div>
				<div class="field">
				    <label>Direccion E-mail:</label>
				    {!!Form::email('email', null)!!}
				</div>
				<div class="field">
				    <label>Categoría</label>
			          {!!Form::select('favorito', [
			          '1' => 'Articulos Exclusivos',
			          '2' => 'Belleza y Estetica',
			          '3' => 'Entretenimiento',
			          '4' => 'Hoteles y Turismo',
			          '5' => 'Mascotas',
			          '6' => 'Niños y Bebes',
			          '7' => 'Restaurantes y Cafeterías',
			          '8' => 'Salud',
			          '9' => 'Servicios Automotrices',
			          '10' => 'Servicios Corporativos',
			          '11' => 'Tiendas'
			          ])!!}
				</div>
				<button class="ui button orange" type="submit">Activar</button>
			{!!Form::close()!!}
		</div>
	</div>
@endsection