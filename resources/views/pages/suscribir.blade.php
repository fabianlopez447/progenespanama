
@extends('layouts.template')
@section('titulo', 'Suscribirse')
@section('contenido')
	<div class="ui container">
	<br>
	@if(count($errors)> 0)
<div class="ui inverted red segment">
  <ul>ERROR<hr>
  	 @foreach($errors->all() as $error)
  	 			
  	 			<li>{{$error}}</li>
  	 @endforeach
  </ul>
</div>
@endif
		<div class="ui segment">
			{!!Form::open(['route'=>'admin.suscriptor.store', 'method'=>'POST', 'class'=>'ui form'])!!}
				<div class="field">
				    <label>Nombres</label>
				    {!!Form::text('nombre', null)!!}
				</div>
				<div class="field">
				    <label>Apellidos</label>
				    {!!Form::text('apellido', null)!!}
				</div>
				<div class="field">
				    <label>Direccion E-mail:</label>
				    {!!Form::email('email', null)!!}
				</div>
				<div class="field">
				    <label>¿Qué te interesa más?</label>
			          {!!Form::select('favorito', [
			          '1' => 'Articulos Exclusivos',
			          '2' => 'Belleza y Estetica',
			          '3' => 'Entretenimiento',
			          '4' => 'Hoteles y Turismo',
			          '5' => 'Mascotas',
			          '6' => 'Niños y Bebes',
			          '7' => 'Restaurantes y Cafeterías',
			          '8' => 'Salud',
			          '9' => 'Servicios Automotrices',
			          '10' => 'Servicios Corporativos',
			          '11' => 'Tiendas'
			          ])!!}
				</div>
				<button class="ui button orange" type="submit">Suscribirse</button>
			{!!Form::close()!!}
		</div>
	</div>
@endsection