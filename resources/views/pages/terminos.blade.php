@extends('layouts.template')
@section('titulo', 'Politicas de privaciad')
@section('contenido')
<div class="ui container" style="margin-top: 50px;">
	<div class="ui segment">
			<h1><strong>Aceptaci&oacute;n de T&eacute;rminos y Condiciones</strong></h1>

<p style="font-size:16px;">Progenes Panamá es un portal web enfocado en la venta de productos Agropecuarios y la suministración de información del mismo. El ingreso al sitio supone la aceptación de los Términos y Condiciones.
 </p>
 <p style="font-size:16px;">
<b>Información general</b><br>
Los productos o servicios ofrecidos en este sitio web tendrán una permeancia infinita. Cada producto ofrecido tendrá especificado su precio y descripción detallada del producto antes de hacer la compra y en los servicios podrán encontrar toda la información detallada y un formulario para que nos contacten ante cualquier duda o interés. En la sección de Alta Genetics ofrecemos información a través de otra sitio web vinculando nuestra producto con el mismo.  
       </p>
<p style="font-size:16px;">
<b>Modificaciones</b><br>
Progenes Panamá se reserva el derecho de modificar los Términos y Condiciones de manera unilateral, publicando los cambios realizados por correo.
       </p>
<p style="font-size:16px;">
<b>Privacidad</b><br>
Los datos personales entregados por el usuario son de responsabilidad de quien los aporta. La información será utilizada por © Progenes S.A para crear cuentas personales, informar del producto o servicio a ofrecer. Progenes Panamá se reserva el derecho de utilizar estos datos para informar vía correo electrónico de promociones y ofertas. El usuario podrá darse de baja de estos correos cuando desee.
       </p>
<p style="font-size:16px;">
Para más detalles sobre nuestras políticas de privacidad dirígete a la sección de “Políticas de privacidad”.
Propiedad intelectual
Todos los contenidos, imágenes, diseños, gráficas, logos, etc. del sitio están protegidos por leyes de Propiedad Intelectual. Está prohibido su uso o copia, total o parcial.
       </p>
	</div>
</div>
@endsection