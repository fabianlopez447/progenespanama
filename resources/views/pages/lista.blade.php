@extends('layouts.template')

@section('titulo')
@if($cate == 1)
	Articulos Exclusivos
@endif
@if($cate == 2)
	Belleza y Estetica
@endif
@if($cate == 3)
	Entretenimiento
@endif
@if($cate == 4)
	Hoteles y Turismo
@endif
@if($cate == 5)
	Mascotas
@endif
@if($cate == 6)
	Niños y Bebes
@endif
@if($cate == 7)
	Restaurantes y Cafeterías
@endif
@if($cate == 8)
	Salud
@endif
@if($cate == 9)
	Servicios Automotrices
@endif
@if($cate == 10)
	Servicios Corporativos
@endif
@if($cate == 11)
	Tiendas
@endif
@endsection

@section('style')
{!!Html::style('files/components/message.min.css')!!}
{!!Html::style('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css')!!}
@endsection

@section('contenido')

<?php 
// Original PHP code by Chirp Internet: www.chirp.com.au
// Please acknowledge use of this code by including this header. 
	function myTruncate($string, $limit, $break=".", $pad="…") {
		// return with no change if string is shorter than $limit 
		if(strlen($string) <= $limit)
			return $string;
		// is $break present between $limit and the end of the string? 
		if(false !== ($breakpoint = strpos($string, $break, $limit))) {
			if($breakpoint < strlen($string) - 1) {
				$string = substr($string, 0, $breakpoint).$pad;
			}
		}
		return $string;
	}
?>

<div class="ui grid container" style="margin-top:50px;">
	<h2 class="ui header">
    @if($cate == 1)
    <i class="icon diamond left floated"></i>
  		<div class="content">
		Articulos Exclusivos
		</div>
	@endif
	@if($cate == 2)
	<i class="icon eye"></i>
  		<div class="content">
		Belleza y Estetica
		</div>
	@endif
	@if($cate == 3)
	<i class="icon puzzle"></i>
  		<div class="content">
		Entretenimiento
		</div>
	@endif
	@if($cate == 4)
	<i class="icon hotel"></i>
  		<div class="content">
		Hoteles y Turismo
		</div>
	@endif
	@if($cate == 5)
	<i class="icon paw"></i>
  		<div class="content">
		Mascotas
		</div>
	@endif
	@if($cate == 6)
	<i class="icon child"></i>
  		<div class="content">
		Niños y Bebes
		</div>
	@endif
	@if($cate == 7)
	<i class="icon food"></i>
  		<div class="content">
		Restaurantes y Cafeterías
		</div>
	@endif
	@if($cate == 8)
	<i class="icon heartbeat"></i>
  		<div class="content">
		Salud
		</div>
	@endif
	@if($cate == 9)
	<i class="icon car"></i>
  		<div class="content">
		Servicios Automotrices
		</div>
	@endif
	@if($cate == 10)
		<i class="icon building outline"></i>
  		<div class="content">
		Servicios Corporativos
		</div>
	@endif
	@if($cate == 11)
	<i class="icon shopping bag"></i>
  		<div class="content">
		Tiendas
		</div>
	@endif

	</h2>

<div class="ui segment sixteen wide column">
<div class="ui divided items ">
<?php $existe = false; ?>
@foreach($desc as $Descuento)
 @if(date('d/m/Y', strtotime($Descuento->fecha_vencimiento)) > date('d/m/Y'))
  <div class="item">
    <div class="ui large image">
    	<a class="ui red right corner massive label">
            <p style="transform: rotate(45deg);-moz-transform : rotate(45deg);-webkit-transform : rotate(45deg);-o-transform : rotate(45deg);margin-top:10px;margin-left:20px;margin-bottom:0px;font-size:40px;">{!!$Descuento->descuento!!}%</p>
        </a>
      <img src="{!!asset('descuentos/'.$Descuento->imagen)!!}">
    </div>
    <div class="content">
      <div class="header">{!!$Descuento->nombre!!}</div>
      <div class="meta">
        <span class="price"></span>
        <span class="stay">{!!date("d/m/Y",strtotime($Descuento->created_at))!!}</span>
      </div>
      <div class="description">
        <p>{!!myTruncate($Descuento->descripcion,256, ".", "...")!!}</p>
      </div>
      <div class="extra">
        <a href="{!!asset('descuento/'.$Descuento->id.'/detalles')!!}" class="ui right floated primary button">
          Ver descuento
          <i class="right chevron icon"></i>
        </a>
      </div>
    </div>
  </div>
  @else
  	<div class="ui icon message">
	  <i class="warning circle icon"></i>
	  <div class="content">
		<div class="header">
		  Sin resultados
		</div>
		<p>No existe resultado para {!!$request['search']!!}.</p>
	  </div>
	</div>
  @endif
  <?php $existe = true ?>
  @endforeach
  @if($existe)
  	{!!$desc->render()!!}
  @endif
  @if(!$existe)
  <div class="ui icon message">
	  <i class="warning circle icon"></i>
	  <div class="content">
		<div class="header">
		  Sin articulos
		</div>
		  <p>Esta categoria aún no cuenta con descuentos.</p>
	  </div>
	</div>
  @endif
</div>
</div>
</div>

@endsection