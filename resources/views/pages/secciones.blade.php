@extends('layouts.template')
@section('titulo', 'Alta Genetics')
@section('contenido')
<div class="container">
  <center><h2>{{$nombre}}</h2></center>
  <div class=" mt-25 mb-25">
    <div class="owl-carousel products">
      @foreach($product as $products)
        <div data-name="{{$products->nombre}}" data-category="mobiles" class="xv-product">
          <figure>
            <a href="{{route('detallesAlta', $products->id)}}"><img style="width:280px; height:280px;" class="xv-superimage" src="{{asset('altagenetics/'.$products->imagen)}}"></a>
          </figure>
          <div class="xv-product-content">
            <h3><a href="{{route('detallesAlta', $products->id)}}">{{$products->nombre}}</a></h3>
            <a  href="{{route('detallesAlta', $products->id)}}" class="product-buy">Ver</a>
          </div>
        </div>
      @endforeach
    </div>
  </div>
  {!! $product->render() !!}
</div>
@endsection