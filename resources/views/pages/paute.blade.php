@extends('layouts.template')
@section('titulo', 'Paute con nosotros')
@section('contenido')
<div class="ui container">
	<div class="ui segment">
		<img src="{!!asset('img/paute1.jpg')!!}" width="100%" alt="">
		<img src="{!!asset('img/paute2.JPG')!!}" width="100%" alt="">
		<img src="{!!asset('img/paute3.JPG')!!}" width="100%" alt="">
	</div>
	<div class="row">
		<div class="col-md-6">
			<center><h2>Contactanos</h2></center>
			{!!Form::open(['route'=>'mail.store', 'method'=>'POST', 'class'=>'ui large form'])!!}
				<div class="ui stacked segment">
					<div class="field">
						<div class="ui left icon input">
							<i class="user icon"></i>
							{!! Form::text('name', null,['placeholder'=>'Nombre y Apellido']) !!}
						</div>
					</div>
					<div class="field">
						<div class="ui left icon input">
							<i class="At icon"></i>
							{!! Form::text('email', null,['placeholder'=>'Dirección E-Mail']) !!}
						</div>
					</div>
					<div class="field">
						<div class="ui left icon input">
							<i class="Idea icon"></i>
							{!! Form::text('asunto', null,['placeholder'=>'Asunto']) !!}
						</div>
					</div>
					<div class="ui form">
						<div class="field">
							{!! Form::textarea('mensaje', null,['placeholder'=>'Mensaje']) !!}
						</div>
					</div>
					<hr>
					<button class="ui right labeled submit orange icon button">
						<i class="right arrow icon"></i>
						Enviar
					</button>
				</div>
				<div class="ui error message"></div>
			{!!Form::close()!!}
   		</div>
   		<div style="margin-top:50px;" class="col-md-6">
	  		<div style="margin-top:12%;"  class="col-md-12">
	  			<div class="col-md-4 offset-md-2">
   					<img width="80%" src="{!!asset('img/contacto.png')!!}" >
   				</div>
   				<div style="margin-top:10px;"  class="col-md-6">
						<b>058-566823352</b><br>
						<b>058-566823352</b><br>
						<b>058-566823352</b>
   				</div>
	  		</div>
   			<div style="margin-top:30px;margin-bottom:30px;"  class="col-md-12">
   				<div class="col-md-4">
					<a href="https://www.facebook.com/Super-Descuentos-Panamá-997108323749863/?ref=ts&fref=ts">
						<img class="btnredes" src="{!!asset('img/FacebookIcon.png')!!}" >
					</a>
   				</div>
   				<div class="col-md-4">
					<a href="https://www.instagram.com/superdescuentospty">
						<img class="btnredes" src="{!!asset('img/InstragramIcon.png')!!}" >
					</a>
   				</div>
   				<div class="col-md-4">
					<a href="#">
						<img class="btnredes" src="{!!asset('img/YoutubeIcon.png')!!}" >
					</a>
   			</div>
   		</div>
   		<div style="margin-top:12%;">	
   				<center>
   				<img width="10%" src="{!!asset('img/gmail.png')!!}" ><h6 style="font-size:25px;">Superdescuentospanama@gmail.com</h6>		
   				</center>
   				</div>
   		</div>	
   				
	</div>
</div>
@endsection