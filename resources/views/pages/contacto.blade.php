@extends('layouts.template')
@section('titulo', 'Politicas de privaciad')
@section('contenido')
<div class="ui " style="padding-top: 4em; padding-bottom: 4em;">
	<div class="row">
		<div class="col-sm-6 col-md-offset-3">
			 <h1 class="text-center"><span>Contacta con Nosotros</span></h1>
			{!!Form::open(['route'=>'contacto.store','method'=>'POST', 'class'=>''])!!}
			<form>
		        <div class="row">
                  <div class="col-md-6">
                             <div class="col-xs-12">
                                 {!!Form::text('nombre', null,['required', 'class'=>'' ,'placeholder'=>'Nombre y Apellido'])!!}
                             </div><!--column-->

                             <div class="col-xs-12">
                                 {!!Form::text('asunto', null,['required', 'class'=>'' ,'placeholder'=>'Asunto'])!!}
                             </div><!--column-->

                             <div class="col-xs-12">
                                 {!!Form::email('email', null,['required', 'class'=>'' ,'placeholder'=>'Email'])!!}
                             </div><!--column-->

                             <div class="col-xs-12">
                                 {!!Form::textarea('mensaje', null,['required', 'class'=>'' ,'placeholder'=>'Mensaje'])!!}
                             </div><!--column-->
                             <div class="col-xs-12">
                                 <button type="submit" class="btn btn-send btn-pink">
                                     <i class="fa fa-paper-plane"></i>
                                     Enviar mensaje
                                 </button>

                             </div>
                             {!!form::close()!!}
                       
                     </div>
                   
                      <div style="margin-top:50px;" class="col-md-6">
                           <div style=""  class="col-md-12">
                              <center>
                               <div class="col-md-6 ">
                                   <img width="60%" src="{!!asset('img/contacto.png')!!}" >
                               </div>
                               <div style="margin-top:5%; font-size:30px;"  class="col-md-6">
                                       <b>6934 5534</b><br>
                                       <b>6822 3066</b
                                       
                               </div>
                               </center>
                           </div>
                              <div style="margin-top:30px;margin-bottom:30px;"  class="col-md-12">
                                  <div class="col-md-4 col-xs-4">
                                      <a href="#">
                                          <img class="btnredes" src="{!!asset('img/FacebookIcon.png')!!}" >
                                      </a>
                                  </div>
                                  <div class="col-md-4 col-xs-4">
                                      <a href="#">
                                          <img class="btnredes" src="{!!asset('img/InstragramIcon.png')!!}" >
                                      </a>
                                  </div>
                                  <div class="col-md-4 col-xs-4">
                                      <a href="#">
                                          <img class="btnredes" src="{!!asset('img/YoutubeIcon.png')!!}" >
                                      </a>
                              </div>
                          </div>
                             <div style="margin-top:12%;">	
                                     <center>
                                     <img width="10%" src="{!!asset('img/gmail.png')!!}" ><h6 style="font-size:25px;">info.progenes@gmail.com</h6>		
                                     </center>
                                     </div>
                            
    
    </div>

                   </div>
		</div>
		

	</div>
</div>
@endsection