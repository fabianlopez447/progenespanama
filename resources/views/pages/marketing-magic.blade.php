@extends('layouts.template')
@section('titulo', 'Paute con nosotros')
@section('contenido')
<div class="ui container">
	<div class="ui segment">
		<div style="text-align: center;"><img width="70%" src="{!!asset('img/marketing-magic.JPG')!!}" alt="" /></div><br>
<div style="margin-bottom: 0.35cm; line-height: 115%; text-align: justify;">Est&aacute; comprobado que los medios digitales, como redes sociales, mensajer&iacute;a de WhatsApp, p&aacute;gina web de la empresa y correos electr&oacute;nicos, son herramientas de gran impacto para el posicionamiento y generar confianza en los clientes, sin embargo, debemos alimentar estos medios de comunicaci&oacute;n con informaci&oacute;n real, noticiosa y con buen contenido.  Estudios de percepci&oacute;n revelan que el 80% de los usuarios de medios digitales no se enganchan a un producto o servicio por la publicidad, pero el 100% consume contenido e informaci&oacute;n de los productos, servicios y temas que le interesan y aceptan videos y noticias de las compa&ntilde;&iacute;as que siguen, por eso, generar contenido noticioso en varios formatos es de vital importancia para que la empresa cree un v&iacute;nculo de fidelizaci&oacute;n con sus clientes.  &iquest;Est&aacute; su empresa utilizando el poder del contenido?</div>
<div style="margin-bottom: 0.35cm; line-height: 115%">&iexcl;Marketing Magic le puede ayudar!</div>
<div style="margin-bottom: 0.35cm; line-height: 115%" align="center"><b>PAQUETES DE MARKETING DE CONTENIDO</b></div>
<ol>
    <li>
    <div style="margin-bottom: 0.35cm; line-height: 115%">COBERTURA DE EVENTOS:  Incluye la fotograf&iacute;a noticiosa del evento, 	redacci&oacute;n de noticia, video noticia editada con locuci&oacute;n 	profesional, publicaci&oacute;n de su noticia en nuestro peri&oacute;dico 	digital Panam&aacute; Business News y reenv&iacute;o a sus redes sociales</div>
    </li>
    <li>
    <div style="margin-bottom: 0.35cm; line-height: 115%">TRANSMISI&Oacute;N Y COBERTURA:  Incluye un env&iacute;o de e-flyer a su base de 	datos de clientes o a nuestra base de datos de empresas y ejecutivos 	(5,000 correos), un equipo con fot&oacute;grafo y redactor para 	transmisi&oacute;n en vivo por redes sociales, res&uacute;menes del evento en 	tiempo real con fotograf&iacute;as, redacci&oacute;n noticiosa, edici&oacute;n de 	video noticia con locuci&oacute;n profesional (a entregarse posterior al 	evento) entrevistas y testimoniales.</div>
    </li>
    <li>
    <div style="margin-bottom: 0.35cm; line-height: 115%">Servicio de Maestros de Ceremonia:  su evento debe tener la mejor 	presentaci&oacute;n, por eso contamos con las mejores voces femeninas y 	masculinas, todos locutores profesionales con amplia experiencia en 	radio y actividades internacionales en el campo de la industria, 	medicina, contabilidad, negocios, gobierno, cultura y actividades 	acad&eacute;micas, quienes est&aacute;n a su disposici&oacute;n para dar el toque de 	elegancia a su congreso, reuni&oacute;n, conferencia y festividad 	corporativa.</div>
    </li>
    <li>
    <div style="margin-bottom: 0.35cm; line-height: 115%">Servicio de Fotograf&iacute;a:  capturamos todos los momentos de 	importancia en su actividad, para que tenga un v&iacute;vido recuerdo, 	contamos con un equipo de fot&oacute;grafos profesionales que har&aacute;n todas 	las tomas para usted, tanto en actividades sociales como 	empresariales.</div>
    </li>
    <li>
    <div style="margin-bottom: 0.35cm; line-height: 115%">Video:  d&iacute;ganos c&oacute;mo desea su video, con 	grabaciones posteriores al evento, entrevistas, testimoniales y 	edici&oacute;n de los diversos momentos; con locuci&oacute;n profesional; con 	m&uacute;sica creada especialmente para el video; o requiere de varias 	c&aacute;maras para captar todos los detalles y transmitirlos por 	pantallas en vivo&hellip; lo que usted desee, nosotros lo cumplimos.</div>
    </li>
    <li>
    <div style="margin-bottom: 0.35cm; line-height: 115%">Env&iacute;o masivo:  contamos con una base de datos comprobada de 25,000 	personas, clase AB, ejecutivos, profesionales, CEO, gerentes y 	due&ntilde;os de negocios.  El env&iacute;o incluye la producci&oacute;n del e-flyer  	y env&iacute;o por WhatsApp y correo electr&oacute;nico</div>
    </li>
    <li>
    <div style="margin-bottom: 0.35cm; line-height: 115%">Redacci&oacute;n de noticia:  haga que su target conozca detalles de 	inter&eacute;s sobre sus productos, servicios y actualidad.</div>
    </li>
    <li>
    <div style="margin-bottom: 0.35cm; line-height: 115%">Redacci&oacute;n de Script &ndash; Reportaje:  el ser humano gusta de escuchar 	y leer historias, por eso, la tendencia en comunicaci&oacute;n es conectar 	nuestra psiquis con historias llenas de contenido interesante, un 	reportaje es una historia que su cliente no olvidar&aacute;.</div>
    </li>
    <li>
    <div style="margin-bottom: 0.35cm; line-height: 115%">Redacci&oacute;n de discursos:   cada palabra cuenta, por eso su discurso 	debe ser envolvente, audaz y hacer sentir al p&uacute;blico que est&aacute; 	frente al mejor, nosotros le ayudamos a crear el discurso que le 	har&aacute; brillar sobre todos sus competidores.</div>
    </li>
    <li>
    <div style="margin-bottom: 0.35cm; line-height: 115%">Locuci&oacute;n:  la voz perfecta para que su noticia, video, redacci&oacute;n o 	reportaje quede resonando en la mente de su target.</div>
    </li>
    <li>
    <div style="margin-bottom: 0.35cm; line-height: 115%">Organizaci&oacute;n de Eventos:  cuente con nuestros expertos para 	ayudarle a organizar todos los detalles que har&aacute;n de su evento el 	m&aacute;s memorable</div>
    </li>
    <li>
    <div style="margin-bottom: 0.35cm; line-height: 115%">Personal de Protocolo:  edecanes, azafatas, acomodadores&hellip; lo que 	necesite lo tenemos.</div>
    <ol>
        <li>
        <div style="margin-bottom: 0.35cm; line-height: 115%">Equipo de 3</div>
        </li>
        <li>
        <div style="margin-bottom: 0.35cm; line-height: 115%">Equipo de 4</div>
        </li>
        <li>
        <div style="margin-bottom: 0.35cm; line-height: 115%">Equipo de 6</div>
        </li>
    </ol>
    </li>
    <li>
    <div style="margin-bottom: 0.35cm; line-height: 115%">Dise&ntilde;o Gr&aacute;fico:  haga que la mente de sus consumidores recuerde de 	forma sencilla su marca.</div>
    </li>
    <li>
    <div style="margin-bottom: 0.35cm; line-height: 115%">Dise&ntilde;o de Logo: establece la identidad corporativa de tu empresa.</div>
    </li>
    <li>
    <div style="margin-bottom: 0.35cm; line-height: 115%">Dise&ntilde;o de valla publicitaria.</div>
    </li>
    <li>
    <div style="margin-bottom: 0.35cm; line-height: 115%">Dise&ntilde;o de Videos.</div>
    </li>
    <li>
    <div style="margin-bottom: 0.35cm; line-height: 115%">Dise&ntilde;o de D&iacute;ptico, Tr&iacute;ptico, Pend&oacute;n y Tarjeta de presentaci&oacute;n.</div>
    </li>
    <li>
    <div style="margin-bottom: 0.35cm; line-height: 115%">Carnets de identificaci&oacute;n:  dele a sus colaboradores el sentido de 	pertenencia mediante un carnet que les otorgue beneficios de 	descuentos en diversos comercios con nuestro programa S&uacute;per 	Descuento; o cree un programa de fidelizaci&oacute;n exclusivo para sus 	clientes con carnets personalizados, impresos directamente en PVC.   	Desde B/.4.00 por carnet.</div>
    </li>
    <li>
    <div style="margin-bottom: 0.35cm; line-height: 115%">Entrevistas y publicidad en Radio:  ponemos a su disposici&oacute;n 	nuestro programa de Radio, Zona VIPTY, que se transmite cada s&aacute;bado 	a la 1 pm por Radio Panam&aacute;, la estaci&oacute;n de radio m&aacute;s escuchada 	por el p&uacute;blico AB de 25 a 50 a&ntilde;os en todo Panam&aacute;, y que es 	seguido por m&aacute;s de 30,000 personas en cada emisi&oacute;n, adem&aacute;s de 	nuestros fans de redes sociales.</div>
    <ol>
        <li>
        <div style="margin-bottom: 0.35cm; line-height: 115%">Entrevista promocional</div>
        </li>
        <li>
        <div style="margin-bottom: 0.35cm; line-height: 115%">Transmisi&oacute;n de cu&ntilde;as y menciones en cada programa durante un mes</div>
        </li>
    </ol>
    </li>
    <li>
    <div style="margin-bottom: 0.35cm; line-height: 115%">MARKETING COMMUNICATION:  creamos las campa&ntilde;as para sus medios de 	comunicaci&oacute;n.</div>
    </li>
    <li>
    <div style="margin-bottom: 0.35cm; line-height: 115%">COBERTURA CONTINUA DE SUS EVENTOS:  Incluye el servicio de Marketing 	Comunication, generaci&oacute;n de contenido noticioso y cobertura 	completa de todos sus eventos, poniendo a su disposici&oacute;n a un 	equipo completo de trabajo para su empresa.  Es tener un 	departamento completo de Relaciones P&uacute;blicas.</div>
    </li>
</ol>
<div style="margin-bottom: 0.35cm; line-height: 115%"><br />
&nbsp;</div>
<div style="margin-bottom: 0.35cm; line-height: 115%"><br />
&nbsp;</div>
<div style="margin-left: 1.27cm; margin-bottom: 0.35cm; line-height: 115%" align="center"><font style="font-size: 14pt" size="4"><b>Decida hoy c&oacute;mo quiere que sea el futuro de su empresa, decida </b></font></div>
<div style="margin-left: 1.27cm; margin-bottom: 0.35cm; line-height: 115%" align="center"><font style="font-size: 14pt" size="4"><b>hoy por el mejor servicio de comunicaci&oacute;n corporativa, </b></font></div>
<div style="margin-left: 1.27cm; margin-bottom: 0.35cm; line-height: 115%" align="center"><font style="font-size: 14pt" size="4"><b>Marketing Magic.</b></font></div>
	</div>
    <!-- Contacto -->
    <div class="row">
        <div class="col-md-6">
            <center><h2>Contactanos</h2></center>
            {!!Form::open(['route'=>'mail.store', 'method'=>'POST', 'class'=>'ui large form'])!!}
                <div class="ui stacked segment">
                    <div class="field">
                        <div class="ui left icon input">
                            <i class="user icon"></i>
                            {!! Form::text('name', null,['placeholder'=>'Nombre y Apellido']) !!}
                        </div>
                    </div>
                    <div class="field">
                        <div class="ui left icon input">
                            <i class="At icon"></i>
                            {!! Form::text('email', null,['placeholder'=>'Dirección E-Mail']) !!}
                        </div>
                    </div>
                    <div class="field">
                        <div class="ui left icon input">
                            <i class="Idea icon"></i>
                            {!! Form::text('asunto', null,['placeholder'=>'Asunto']) !!}
                        </div>
                    </div>
                    <div class="ui form">
                        <div class="field">
                            {!! Form::textarea('mensaje', null,['placeholder'=>'Mensaje']) !!}
                        </div>
                    </div>
                    <hr>
                    <button class="ui right labeled submit orange icon button">
                        <i class="right arrow icon"></i>
                        Enviar
                    </button>
                </div>
                <div class="ui error message"></div>
            {!!Form::close()!!}
        </div>
    
    	<div style="margin-top:50px;" class="col-md-6">
	  		<div style="margin-top:12%;"  class="col-md-12">
	  			<div class="col-md-4 offset-md-2">
   					<img width="80%" src="{!!asset('img/contacto.png')!!}" >
   				</div>
   				<div style="margin-top:10px;"  class="col-md-6">
						<b>058-566823352</b><br>
						<b>058-566823352</b><br>
						<b>058-566823352</b>
   				</div>
	  		</div>
   			<div style="margin-top:30px;margin-bottom:30px;"  class="col-md-12">
   				<div class="col-md-4">
					<a href="https://www.facebook.com/Super-Descuentos-Panamá-997108323749863/?ref=ts&fref=ts">
						<img class="btnredes" src="{!!asset('img/FacebookIcon.png')!!}" >
					</a>
   				</div>
   				<div class="col-md-4">
					<a href="https://www.instagram.com/superdescuentospty">
						<img class="btnredes" src="{!!asset('img/InstragramIcon.png')!!}" >
					</a>
   				</div>
   				<div class="col-md-4">
					<a href="#">
						<img class="btnredes" src="{!!asset('img/YoutubeIcon.png')!!}" >
					</a>
   			</div>
   		</div>
   		<div style="margin-top:12%;">	
   				<center>
   				<img width="10%" src="{!!asset('img/gmail.png')!!}" ><h6 style="font-size:25px;">marketingmagicpanama@gmail.com</h6>		
   				</center>
   				</div>
   		</div>	
    
    </div>
</div>
@endsection