@extends('layouts.template')
@section('titulo', 'Bienvenido')

@section('contenido')
@include('alerts.warning')
<div class="ui grid">

<?php 
// Original PHP code by Chirp Internet: www.chirp.com.au
// Please acknowledge use of this code by including this header. 
	function myTruncate($string, $limit, $break=".", $pad="…") {
		// return with no change if string is shorter than $limit 
		if(strlen($string) <= $limit)
			return $string;
		// is $break present between $limit and the end of the string? 
		if(false !== ($breakpoint = strpos($string, $break, $limit))) {
			if($breakpoint < strlen($string) - 1) {
				$string = substr($string, 0, $breakpoint).$pad;
			}
		}
		return $string;
	}
?>
	
<!--========================================
Slider Area
===========================================-->
 <div class="container">
        <div class="product-overview pt-25 pb-45">
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <section class="new-product"> 
                        <header class="sec-heading text-center">
                            <div class="category-wrap">
                               
                                <ul class="cat-dropDown">
                                    <li><a href="#">Artículos Exclusivos</a></li>
                                    <li><a href="#">Entretenimiento</a></li>
                                    <li><a href="#">Belleza</a></li>
                                </ul>
                            </div>
                            <h2><span>Catalogo BMV Technology</span></h2>
                           
                        </header><!--header-->
                        <div class="container">
                            <div class=" mt-25 mb-25">
                            <div class="owl-carousel products">
                                    @foreach($product as $products)
                                    <div data-name="{{$products->nombre}}" data-category="mobiles" class="xv-product">
                                        <figure>
                                            <a href="{{route('detallesBmv', $products->id)}}"><img style="width:280px; height:280px;" class="xv-superimage" src="{{asset('bmv/'.$products->imagen)}}"></a>
                                        </figure>
                                        <div class="xv-product-content">
                                            <h3><a href="{{route('detallesBmv', $products->id)}}">{{$products->nombre}}</a></h3>
                                            <span class="xv-price">{{$products->precio}}</span>
                                            <a  href="{{route('detallesBmv', $products->id)}}" class="product-buy">Ver</a>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            {!! $product->render() !!}
                        </div>
                    </section>
                </div><!--co-->
            </div><!--row-->
        </div><!--product-overview -->
    </div><!--container-->

    </div><!--slider Wrap-->
@endsection

@section('js')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script>
   
  </script>
@endsection