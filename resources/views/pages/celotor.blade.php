@extends('layouts.template')
@section('titulo', 'Bienvenido')

@section('contenido')
@include('alerts.warning')
<div class="ui grid">

<?php 
// Original PHP code by Chirp Internet: www.chirp.com.au
// Please acknowledge use of this code by including this header. 
	function myTruncate($string, $limit, $break=".", $pad="…") {
		// return with no change if string is shorter than $limit 
		if(strlen($string) <= $limit)
			return $string;
		// is $break present between $limit and the end of the string? 
		if(false !== ($breakpoint = strpos($string, $break, $limit))) {
			if($breakpoint < strlen($string) - 1) {
				$string = substr($string, 0, $breakpoint).$pad;
			}
		}
		return $string;
	}
?>
	
<!--========================================
Slider Area
===========================================-->
 <div class="container">
     <div class="container-fluid hidden-xs hidden-sm" style="min-width: 70%; width: 1024px; max-width:100%; margin-left: 0 auto; margin-right: 0 auto;">
       <div id="carousel-example-generic" style="height: 500px;" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            <li data-target="#carousel-example-generic" data-slide-to="3"></li>
          </ol>
          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <img style="height: 500px;" src="{{asset('img/celotor1.jpg')}}" alt="...">
              <div class="carousel-caption">
                ...
              </div>
            </div>
            <div class="item">
              <img style="height: 500px;" src="{{asset('img/celotor2.jpg')}}" alt="...">
              <div class="carousel-caption">
                ...
              </div>
            </div>
            <div class="item">
              <img style="height: 500px;" src="{{asset('img/celotor3.jpg')}}" alt="...">
              <div class="carousel-caption">
                ...
              </div>
            </div>
            <div class="item">
              <img style="height: 500px;" src="{{asset('img/celotor4.jpg')}}" alt="...">
              <div class="carousel-caption">
                ...
              </div>
            </div>
          </div>
          <!-- Controls -->
          <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>

      </div>
      
      <!-- SLIDER SMALLL-->
       <div class="container-fluid visible-xs visible-sm" style="min-width: 70%; width: 1024px; max-width:100%; margin-left: 0 auto; margin-right: 0 auto;">
       <div id="carousel-example-generic1" style="height: 200px;" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic1" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic1" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic1" data-slide-to="2"></li>
            <li data-target="#carousel-example-generic1" data-slide-to="3"></li>
          </ol>
          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <img style="height: 200px;" src="{{asset('img/celotor1.jpg')}}" alt="...">
              <div class="carousel-caption">
                ...
              </div>
            </div>
            <div class="item">
              <img style="height: 200px;" src="{{asset('img/celotor2.jpg')}}" alt="...">
              <div class="carousel-caption">
                ...
              </div>
            </div>
            <div class="item">
              <img style="height: 200px;" src="{{asset('img/celotor3.jpg')}}" alt="...">
              <div class="carousel-caption">
                ...
              </div>
            </div>
            <div class="item">
              <img style="height: 200px;" src="{{asset('img/celotor4.jpg')}}" alt="...">
              <div class="carousel-caption">
                ...
              </div>
            </div>
          </div>
          <!-- Controls -->
          <a class="left carousel-control" href="#carousel-example-generic1" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#carousel-example-generic1" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>

      </div>
           <!-- /SLIDER SMALLL-->
      
      <div class="row" style="margin-top:2em;">
        <div class="col-xs-12">
          <h1 class="texto1" style="color:#EDB32A">¿QUÉ ES CELOTOR?</h1>
        </div>
        <div class="col-md-6">
          <img src="{{asset('img/pasoscelo.jpg')}}" alt="">
        </div>
        <div class="col-md-6"> <br><br><br>
          <p class="celotor-text">CELOTOR  es un detector de celo bovino que detecta el “standing” o monta de las vacas en celo, envía mensajes de texto al inseminador y guarda registros en la web.</p>
        </div>
      </div>      
      <div class="row">
        <br>
         <h1 class="texto1" style="color:#EDB32A">PARTES Y OPERACIÓN</h1>
         <br>
         <p class="celotor-text" >Toda vaca en celo es montada por sus compañeras o por el toro, la vaca o toro que salta tendrá puesto el cinturón
         lector en el pecho y la vaca que se deja montar tendrá eñ chip inyectado en la base de la cola.
         <p class="celotor-text" >
         Durante la monta o standing un animal está arriba del que se deja montar, de tal forma que el pecho del animal que monta queda sobre la base de la cola del que se deja montar
         y por lo tanto el cinturón lector queda sobre el chip inyectado
         </p>
           <p class="celotor-text" >
         De esta forma el cinturon lee la identificación de la vaca en celo. Esta indentificación es enviada inalámbricamente al Collar Maestro 
         y este a su vez origina alertas mediante mensajes de texto en tiempo real. También se podrá ver la información en correos electrónicos y en un sitio web.
         </p>
      </div>
      
       <div class="row">
        <br>
         <h1 class="texto1" style="color:#EDB32A">PARTES</h1>
         <br>
         <p class="celotor-text" >1.   Chip inyectable: uno para cada vaca.</p>
         <p class="celotor-text" >1.   Cinturón Lector: en todas las vacas machorras y toros celadores cuando los hay o simplemente en una de cada tres vacas.</p>
         <p class="celotor-text" >1.   Collar Maestro: uno en cada lote, aproximadamente uno por cada 40 vacas.</p>
   
      <div class="carrusel">
          <div class="row">
            <!--<div class="col-md-2">
             <a class="thumbnail" data-target="#img1" data-toggle="modal" href="#"><img src="{{asset('img/celotor.png')}}" style="width:150px; height:150px;" alt=""></a>
             </div>
             <div class="col-md-2">
             <a class="thumbnail" data-target="#img2" data-toggle="modal" href="#"><img src="{{asset('img/celotor.png')}}" style="width:150px; height:150px;" alt=""></a>
             </div>
             <div class="col-md-2">
             <a class="thumbnail" data-target="#img3" data-toggle="modal" href="#"><img src="{{asset('img/celotor.png')}}" style="width:150px; height:150px;" alt=""></a>
             </div>
             <div class="col-md-2">
             <a class="thumbnail" data-target="#img4" data-toggle="modal" href="#"><img src="{{asset('img/celotor.png')}}" style="width:150px; height:150px;" alt=""></a>
             </div>
             <div class="col-md-2">
             <a class="thumbnail" data-target="#img5" data-toggle="modal" href="#"><img src="{{asset('img/celotor.png')}}" style="width:150px; height:150px;" alt=""></a>
             </div>
             <div class="col-md-2">
             <a class="thumbnail" data-target="#img6" data-toggle="modal" href="#"><img src="{{asset('img/celotor.png')}}" style="width:150px; height:150px;" alt=""></a>
             </div>-->
             
             <!-- Modal  -->
               <div class="modal fade" id="img1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                 <div class="modal-dialog modal-lg" >
                  <div class="modal-content" style="background:none; margin-top:40%;">
                   <div class="modal-body">
                     <img  src="{{asset('img/celotor.png')}}"  alt="">
                   </div>
                 </div>
                  </div>
               </div>
               
               <!-- Modal  -->
               <div class="modal fade" id="img2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" >
                  <div class="modal-content" style="background:none; margin-top:40%;">
                   <div class="modal-body">
                     <img  src="{{asset('img/celotor.png')}}"  alt="">
                   </div>
                 </div>
                  </div>
               </div>
               
               <!-- Modal  -->
               <div class="modal fade" id="img3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                 <div class="modal-dialog modal-lg" >
                  <div class="modal-content" style="background:none; margin-top:40%;">
                   <div class="modal-body">
                     <img  src="{{asset('img/celotor.png')}}"  alt="">
                   </div>
                 </div>
                  </div>
               </div>
               
               <!-- Modal  -->
               <div class="modal fade" id="img4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" >
                  <div class="modal-content" style="background:none; margin-top:40%;">
                   <div class="modal-body">
                     <img  src="{{asset('img/celotor.png')}}"  alt="">
                   </div>
                 </div>
                  </div>
               </div>
               
               <!-- Modal  -->
               <div class="modal fade" id="img5" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                 <div class="modal-dialog modal-lg" >
                  <div class="modal-content" style="background:none; margin-top:40%;">
                   <div class="modal-body">
                     <img  src="{{asset('img/celotor.png')}}"  alt="">
                   </div>
                 </div>
                  </div>
               </div>
               
               <!-- Modal  -->
               <div class="modal fade" id="img6" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                 <div class="modal-dialog modal-lg" >
                  <div class="modal-content" style="background:none; margin-top:40%;">
                   <div class="modal-body">
                     <img  src="{{asset('img/celotor.png')}}"  alt="">
                   </div>
                 </div>
                  </div>
               </div>  
          </div> 
      </div>
   </div>
    <div class="row">
        <div class="col-sm-6">
        
        </div>
         <h1 class="texto1" style="color:#EDB32A">INSTALACIÓN Y USO</h1>

         <p class="celotor-text" >-   Cada chip inyectable es programado con la identificación (nombre y/o número) de cada vaca de acuerdo a un listado que el ganadero suministra antes de la compra junto con el número de teléfono celular en el cual recibirá las alertas..</p>
         <p class="celotor-text" >-   Inyectar los chips a todas las vacas mediante un procedimiento simple rápido y seguro (ver video e instrucciones)</p>
         <p class="celotor-text" >-   Instalar los cinturones a las vacas machorras y toros celadores cuando los hay, o simplemente a una de cada tres vacas del hato.</p>
         <p class="celotor-text" >-   Instalar el collar maestro a una sola vaca de cada lote (ver video).</p>
         <p class="celotor-text" >-   El sistema está listo para monitorear el celo 24 horas al día! basta con estar atento a los mensajes de texto. </p>
         
    </div>
    <div class="row" style="margin-bottom:2em;margin-top:2em;" >
      <div class="col-md-6">
          <iframe width="auto" height="600px" src="https://www.youtube-nocookie.com/embed/zm5fsAh5xqw?controls=0" frameborder="0" allowfullscreen></iframe>
      </div>
      <div class="col-sm-6">
        <h1 class="text-left"><span>Contacto Celotor</span></h1>
        {!!Form::open(['route'=>'contacto.store','method'=>'POST', 'class'=>''])!!}
        <form>
              <div class="row">
                  <div class="col-xs-12">
                    {!!Form::text('nombre', null,['required', 'class'=>'' ,'placeholder'=>'Nombre y Apellido'])!!}
                  </div><!--column-->

                  <div class="col-xs-12">
                    {!!Form::text('asunto', 'Contacto Celotor',['required', 'class'=>'' ,'placeholder'=>'Asunto'])!!}
                  </div><!--column-->

                  <div class="col-xs-12">
                    {!!Form::email('email', null,['required', 'class'=>'' ,'placeholder'=>'Email'])!!}
                  </div><!--column-->

                  <div class="col-xs-12">
                    {!!Form::textarea('mensaje', null,['required', 'class'=>'' ,'placeholder'=>'Mensaje'])!!}
                  </div><!--column-->
                  <div class="col-xs-12">
                  <button type="submit" class="btn btn-send btn-pink">
                    <i class="fa fa-paper-plane"></i>
                Enviar mensaje
              </button>
                    
                  </div>
          {!!form::close()!!}
      </div>
	</div>
    
     
 </div>
@endsection

@section('js')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script>
   
  </script>
@endsection