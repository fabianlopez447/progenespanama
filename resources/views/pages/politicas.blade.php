@extends('layouts.template')
@section('titulo', 'Politicas de privaciad')
@section('contenido')
<div class="ui container" style="margin-top: 50px;">
	<div class="ui segment">
		<h1>Politica de privacidad</h1>

<p style="font-size:16px;">Gracias por visitar este sitio web. Esperamos nuestro contenido le sea de utilidad e interesante. ©Progenes S.A respeta su derecho a la privacidad en la red cuando use nuestra página y se comunique con nosotros. Aplicamos todas las medidas necesarias para mantener segura toda la información personal que usted nos suministre. Es de suma importancia que usted este informado de que ©Progenes S.A es quien controla los datos de su información personal. Todos estos principios se aplican a todos los controladores de datos pertenecientes a © Progenes S.A. Adjunto al tema se contestará una serie de preguntas que quizás resuelva sus dudas acerca de nuestras políticas de privacidad.
 </p>
 
<h3>P1 - ¿Qué tipo de información personal reúne el sitio web?</h3>
       <p style="font-size:16px;">En cuestión a lo que se necesite, la información que se reúne es proporcional a la que usted nos suministre por cuenta propia. Esta puede ser tan básica como nombre, apellido y correo. Pero tan delicada como dirección física, correos electrónicos números telefónicos, fecha de nacimiento, etc.</p>
 
       <h3>P2 - ¿Cómo Progenes S.A mantiene segura su información?</h3>
       <p style="font-size:16px;">Se aplican todas las medidas posibles para mantener su información segura. Solo los entes autorizados y encargados del sitio son los que obtienen esta información para ser utilizada de las dos formas ya mencionadas.</p>
 
       <h3>P3 - ¿Con qué fines se usa su información personal?</h3>

       <p style="font-size:16px;">La información básica que usted pueda suministrar solo se usara por medios estadísticos, para mantener un registro y un conteo de intereses, de esta forma podemos manejar el contenido del sitio de mejor forma, sabiendo que le interesa a los visitantes. Esta información se obtiene por medio del formulario de suscripción del sitio.</p>

       <h3>P4 - ¿A quién revelara Progenes S.A su información personal y por qué?</h3>
<p style="font-size:16px;">Su información personal nunca será compartida por compañías de terceros que con fines de mercadeo a menos que usted así lo quiera. Para usarla en mercadeo de ProgenesPanama, o cualquier proyecto perteneciente a Progenes S.A, de igual forma usted puede en cualquier momento pedir que su información personal sea borrada por medio de un correo electrónico.
   Su información también puede ser revelada a entidades del gobierno y a las agencies de cumplimiento de a ley o si según nuestro criterio, tal acción es razonablemente necesaria para cumplir con un proceso legal, para responder a acciones o demandas legales o proteger los derechos de Progenes S.A, de sus clientes públicos.</p>
 
       <h3>P5 - Cómo puede ver, verificar, cambiar o borrar la información personal que se tiene de usted?</h3>
       <p style="font-size:16px;">Usted tiene todo el derecho de solicitar su información por medio de un correo electrónico y pedir que hagamos con ella lo que usted decida, ya sea cambiarla o borrarla. Como sea es su información y nosotros aceptamos lo que usted decida hacer con ella.</p>


	</div>
</div>
@endsection