@extends('layouts.template')
@section('titulo', 'Bienvenido')
@section('contenido')
@include('alerts.warning')
<div class="ui grid">

<?php 
// Original PHP code by Chirp Internet: www.chirp.com.au
// Please acknowledge use of this code by including this header. 
  function myTruncate($string, $limit, $break=".", $pad="…") {
    // return with no change if string is shorter than $limit 
    if(strlen($string) <= $limit)
      return $string;
    // is $break present between $limit and the end of the string? 
    if(false !== ($breakpoint = strpos($string, $break, $limit))) {
      if($breakpoint < strlen($string) - 1) {
        $string = substr($string, 0, $breakpoint).$pad;
      }
    }
    return $string;
  }
?>
  <div class="sixteen wide column">
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
       
      <ol class="carousel-indicators">
      <?php $indicator = 0; ?>
        @foreach($slider as $Descuento)
      <li data-target="#carousel-example-generic" data-slide-to="{!!$indicator!!}"></li>
      <?php $indicator++; ?>
      @endforeach
      </ol>
      <div class="carousel-inner" role="listbox">
        @foreach($slider as $Descuento)
          <?php $desc = \App\Descuento::where('carnet_codigo', $Descuento->codigo_descuento)->get() ?>
          @foreach($desc as $Pub)
          <a class="ui red right corner massive label">
            <p style="transform: rotate(45deg);-moz-transform : rotate(45deg);-webkit-transform : rotate(45deg);-o-transform : rotate(45deg);margin-top:10px;margin-left:20px;margin-bottom:0px;font-size:40px;">{!!$Pub->descuento!!}%</p>
          </a>
          <div id="primero" class="carousel-item">
            <img src="{!!asset('carrusel/'.$Descuento->imagen)!!}" alt="First slide">
            <div class="carousel-caption">
              <h3>{!!$Pub->nombre!!}</h3>
              <p>{!!myTruncate($Pub->descripcion,20, ".", "...")!!}</p>
              <p><a href="{!!route('descuento.detalles',$Pub->id)!!}" class="ui button large orange">Ver más</a></p>
            </div>
          </div>
        @endforeach
       @endforeach
      </div>
    </div>
    </div>
</div>
<div class="ui grid">
  <div class="one wide column"></div>
    <div class="fourteen wide column">
      <div class="ui inverted segment center aligned">
          <h2 class="ui header">¿Ya activaste tu carnet de descuento?</h2>
            <h3 class="ui gray header"><sub>Para activarlo escribe tu código aquí:</sub></h3>
            {!!Form::open(['method'=>'GET', 'route'=>'activacion.create'])!!}
              <div class="form-group">
                  <div class="ui large input">
                      {!!Form::text('codigo', null,['placeholder'=>'Ingrese su código aquí...'])!!}
                    </div>
                    <button type="submit" class="ui button large orange">Activar</button>
                </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>

<!-- Versión pequeña -->
<div class="ui grid mobile tablet only" id="mitad">
  <div class="ui sixteen wide column">
      <div class="ui segment" style="background: #1b1c1d;overflow:hidden;">
          <div class="row">
                <h4 class="ui horizontal divider header" style="color: white;">Descuentos de la semana</h4>
                <div class="col-md-12">
                  <div id="card" class="card card-inverse">
                      <a class="ui red right corner massive label">
                          <p style="transform: rotate(45deg);-moz-transform : rotate(45deg);-webkit-transform : rotate(45deg);-o-transform : rotate(45deg);margin-top:10px;margin-left:20px;margin-bottom:0px;font-size:40px;">50%</p>
                      </a>
                      <img class="card-img desaturada" src="img/pollofrito.jpg" width="100%" alt="Card image">
                      <a href="#">
                          <div class="card-img-overlay">
                            <h4 class="card-title" style="text-shadow: 1px 1px 0px black;">Pollo frito todos los dias de las semana</h4>
                          </div>
                      </a>
                    </div>
                    <div id="card" class="card card-inverse">
                      <a class="ui red right corner massive label">
                          <p style="transform : rotate(45deg);-moz-transform : rotate(45deg);-webkit-transform : rotate(45deg);-o-transform : rotate(45deg);margin-top:10px;margin-left:20px;margin-bottom:0px;font-size:40px;">20%</p>
                      </a>
                      <img class="card-img desaturada" src="img/wallpaper2.jpg" width="100%" alt="Card image">
                      <a href="#">
                          <div class="card-img-overlay">
                            <h4 class="card-title" style="text-shadow: 1px 1px 0px black;">Mercancia de ropa deportiva para caballeros</h4>
                          </div>
                      </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="ui grid segment" style="margin-left:10px; margin-right:10px;background: #1b1c1d;overflow:hidden;">
  <div class="ui grid">
      <h4 class="ui horizontal divider header" style="color: white;">Descuentos de la Semana</h4>
      @foreach($Desc as $Descuento)
        @foreach($sem as $Semanal)
          @if($Semanal->id_descuento == $Descuento->id)
        <div class="four wide column">
          <div class="ui card" style="width:100%;">
            <a class="ui red right corner massive label">
              <p style="transform: rotate(45deg);-moz-transform : rotate(45deg);-webkit-transform : rotate(45deg);-o-transform : rotate(45deg);margin-top:10px;margin-left:20px;margin-bottom:0px;font-size:40px;">{!!$Descuento->descuento!!}%</p>
            </a>
            <a class="image" href="{!!route('descuento.detalles',$Descuento->id)!!}">
              <img src="{!!asset('descuentos/'.$Descuento->imagen)!!}">
            </a>
            <div class="content">
              <a class="header">{!!$Descuento->nombre!!}</a>
            </div>
          </div>
        </div>
    @endif
    @endforeach
    @endforeach
</div></div>

@endsection

@section('js')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script>
    $(document).ready(function(){
      $('#primero:first').addClass('active');
    });
  </script>
@endsection