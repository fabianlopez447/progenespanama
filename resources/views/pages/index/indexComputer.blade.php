@extends('layouts.template')
@section('titulo', 'Bienvenido')

@section('contenido')
@include('alerts.warning')

<div class="ui grid">

<?php 
// Original PHP code by Chirp Internet: www.chirp.com.au
// Please acknowledge use of this code by including this header. 
	function myTruncate($string, $limit, $break=".", $pad="…") {
		// return with no change if string is shorter than $limit 
		if(strlen($string) <= $limit)
			return $string;
		// is $break present between $limit and the end of the string? 
		if(false !== ($breakpoint = strpos($string, $break, $limit))) {
			if($breakpoint < strlen($string) - 1) {
				$string = substr($string, 0, $breakpoint).$pad;
			}
		}
		return $string;
	}
?>
<!--========================================
Slider Area
===========================================-->
<div>
<div class="container-fluid hidden-xs hidden-sm" style=" background: none; min-width: 70%; width: 1024px; max-width:100%; margin-left: 0 auto; margin-right: 0 auto;">
    <div id="progenes" style="height: 500px;" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators">
          <?php $active = 'active'; ?>
            @foreach($slider as $slide)
            <li data-target="#progenes" data-slide-to="0" class="{{$active}}"></li>
            <?php $active = ''; ?>
            @endforeach
          </ol>
          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            <?php $activo = 'active'; ?>
            @foreach($slider as $slide)
            <div class="item {{$activo}}">
              <img style="height: 500px;width: 100%;" src="{{asset('carrusel/'.$slide->imagen)}}" alt="{{$slide->codigo_descuento}}">
              <div class="carousel-caption">
                <a href="{{$slide->codigo_descuento}}" class="btn btn-primary">Ver más</a>
              </div>
            </div>
            <?php $activo = ''; ?>
            @endforeach
          </div>

          <!-- Controls -->
          <a class="left carousel-control" href="#progenes" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#progenes" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
    </div>
</div>
  
  <br>
  
  <div class="container-fluid visible-xs visible-sm " style=" background: none; min-width: 70%; width: 1024px; max-width:100%; margin-left: 0 auto; margin-right: 0 auto;">
    <div id="progenes2" style="height: 300px;" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators">
          <?php $active = 'active'; ?>
            @foreach($slider as $slide)
            <li data-target="#progenes2" data-slide-to="0" class="{{$active}}"></li>
            <?php $active = ''; ?>
            @endforeach
          </ol>
          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            <?php $activo = 'active'; ?>
            @foreach($slider as $slide)
            <div class="item {{$activo}}">
              <img style="height: 300px;width: 100%;" src="{{asset('carrusel/'.$slide->imagen)}}" alt="{{$slide->codigo_descuento}}">
              <div class="carousel-caption">
                <a href="{{$slide->codigo_descuento}}" class="btn btn-primary">Ver más</a>
              </div>
            </div>
            <?php $activo = ''; ?>
            @endforeach
          </div>

          <!-- Controls -->
          <a class="left carousel-control" href="#progenes2" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#progenes2" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
    </div>
</div>
   
    <!--Ultimos productos-->
    <div class="container" style="background: none;">
        <div class="xv-bseller pt-25 pb-20">
            <header class="sec-heading text-center">
                <h2><span>Progenes - Ultimos productos</span></h2>
            </header><!--header-->
                <div style="background: none;" class="xv-best-seller">
                    <div class="row">
                        @foreach($progenes as $item)
                        <div class="col-xs-12 col-sm-4 col-md-4 no-pad" style="padding: 5px;">
                            <div style="background: white;border-radius: 5px; border-bottom: 2px solid #7f8c8d; " data-pid="xyz401" data-name="Smartphone Apple iPhone 5 64GB" data-category="mobiles" data-price="250" class="xv-accessories text-center pt-10 pb-30 border-right">

                                <h1>{{$item->nombre}}</h1>
                                <figure style="height: auto;">
                                    <img style="border-radius: 10px;" src="{{asset('progenes/'.$item->imagen)}}" alt="{{$item->nombre}}">
                                    <figcaption style="background: rgba(0,0,0,0.2); transition-duration: 1s">
                                        <ul class="style1">
                                            <li><a class="btn-cart btn-rectangle btn-blue" href="{{route('detallesProgenes', $item->id)}}"><i class="fa fa-shopping-cart"></i>
                                            Ver más</a></li>
                                        </ul>
                                    </figcaption>
                                </figure>
                                <h4 class="header"><span class="text-danger">${{number_format($item->precio,2,",",".")}}</span></h4>
                            </div><!--accessories-->
                        </div><!--column-->
                        @endforeach
                    </div><!--top-row-->
                </div><!--bottom-row-->
            </div><!--xv-best-seller-->
            <!--xv-best-seller-->
        </div>  
    <!--Ultimos productos-->

    <!--Buscador de codigos-->
     <div class="container" style="height: auto; background: white; border-radius: 10px; border-bottom: 2px #888 solid; padding-bottom: 15px; ">
       <center>
         <br>
          <img style=" width:30%;"class="xv-superimage" src="/img/alta.png" alt="">
       </center>
        <div class="xv-sign-up text-center pt-20 pb-60" style="margin-bottom: -100px; height: auto;">
            <h3>Busca tu toro aquí</h3>
            {!!Form::open(['route'=>'codigotoro.ver','method'=>'post', 'class'=>'sign-up-form', 'style' => 'height: auto;'])!!}
                <i class="fa fa-search" style="color: #00bbdd"></i>
                <input type="text" name="codigo" style="border: 1px solid #a9a9a9;" placeholder="Ingresar el codigo">
                <button type="submit" style="background: #00bbdd">Buscar</button>
            {!!Form::close()!!}
        </div><!--xv-signup-->
    </div><!--container-->
    <div class="container" style="margin-top: 0px;">
        <div class="xv-bseller pt-25 pb-20" style="margin-top: 0px;">
            <header class="sec-heading text-center" style="margin-top: 0px;">
                <div class="category-wrap style" style="margin-top: 0px;">          
                </div>
                <h2 style="margin-top: 0px;"><span>Alta Genetics - Ultimos productos</span></h2>
            
            </header><!--header-->
                <div style="background: none;" class="xv-best-seller">
                    <div class="row">
                        @foreach($alta as $item)
                        <div class="col-xs-12 col-sm-4 col-md-4 no-pad" style="padding: 5px;">
                            <div style="background: white;border-radius: 5px; border-bottom: 2px solid #7f8c8d; " data-pid="xyz401" data-name="Smartphone Apple iPhone 5 64GB" data-category="mobiles" data-price="250" class="xv-accessories text-center pt-10 pb-30 border-right">
                              <h1>{{$item->nombre}}</h1>
                                <figure style="height: auto;">
                                    <img style="border-radius: 10px;" src="{{asset('altagenetics/'.$item->imagen)}}" alt="{{$item->nombre}}">
                                    <figcaption style="background: rgba(0,0,0,0.2); transition-duration: 1s">
                                        <ul class="style1">
                                            <li><a class="btn-cart btn-rectangle btn-blue" href="{{route('detallesAlta', $item->id)}}"><i class="fa fa-shopping-cart"></i>
                                            Ver más</a></li>
                                        </ul>
                                    </figcaption>
                                </figure>
                          
                            </div><!--accessories-->
                        </div><!--column-->
                        @endforeach
                    </div><!--top-row-->
                </div><!--bottom-row-->
                </div><!--xv-best-seller-->
                <!--xv-best-seller-->
            </div>  
    <!--Ultimos productos > -->
    
    <!--Ultimos productos-->
    <div class="container">
        <div class="xv-bseller pt-25 pb-20">
            <header class="sec-heading text-center">
                <h2><span>BMVTechnology - Ultimos productos</span></h2>
            </header><!--header-->
                <div style="background: none;" class="xv-best-seller">
                    <div class="row">
                        @foreach($bmv as $item)
                        <div class="col-xs-12 col-sm-4 col-md-4 no-pad" style="padding: 5px;">
                            <div style="background: white;border-radius: 5px; border-bottom: 2px solid #7f8c8d; " data-pid="xyz401" data-name="Smartphone Apple iPhone 5 64GB" data-category="mobiles" data-price="250" class="xv-accessories text-center pt-10 pb-30 border-right">
                              <h1>{{$item->nombre}}</h1>
                                <figure style="height: auto;">
                                    <img style="border-radius: 10px;" src="{{asset('bmv/'.$item->imagen)}}" alt="{{$item->nombre}}">
                                    <figcaption style="background: rgba(0,0,0,0.2); transition-duration: 1s">
                                        <ul class="style1">
                                            <li><a class="btn-cart btn-rectangle btn-blue" href="{{route('detallesBmv', $item->id)}}"><i class="fa fa-shopping-cart"></i>
                                            Ver más</a></li>
                                        </ul>
                                    </figcaption>
                                </figure>
                                <h4 class="header"><span class="text-danger">${{number_format($item->precio,2,",",".")}}</span></h4>
                            </div><!--accessories-->
                        </div><!--column-->
                        @endforeach
                    </div><!--top-row-->
                </div><!--bottom-row-->
            </div><!--xv-best-seller-->
            <!--xv-best-seller-->
        </div>  
    <!--Ultimos productos-->

    <!--Ultimos productos-->
    <div class="container">
        <header class="sec-heading text-center">
                <div class="category-wrap style">          
                </div>
                <h2><span>Celotor </span></h2>
            
            </header><!--header-->
        <div class="container-fluid hidden-xs hidden-sm" style=" background: none; min-width: 70%; width: 1024px; max-width:100%; margin-left: 0 auto; margin-right: 0 auto;">
    <div id="celotorSlider" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <img style="width: 100%;" src="{{asset('img/Celotor%20Paso%201.jpg')}}" alt="Pasos celotor">
              <div class="carousel-caption">
              </div>
            </div>
            
            <div class="item">
              <img style="width: 100%;" src="{{asset('img/Celotor%20Paso%202.jpg')}}" alt="Pasos celotor">
              <div class="carousel-caption">
              </div>
            </div>
            
            <div class="item">
              <img style="width: 100%;" src="{{asset('img/Celotor%20Paso%203.jpg')}}" alt="Pasos celotor">
              <div class="carousel-caption">
              </div>
            </div>
            
            <div class="item">
              <img style="width: 100%;" src="{{asset('img/Celotor%20Paso%204.jpg')}}" alt="Pasos celotor">
              <div class="carousel-caption">
              </div>
            </div>
          </div>

          <!-- Controls -->
          <a class="left carousel-control" href="#celotorSlider" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#celotorSlider" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
    </div>
</div>
<div class="container-fluid visible-xs visible-sm " style=" background: none; min-width: 70%; width: 1024px; max-width:100%; margin-left: 0 auto; margin-right: 0 auto;">
    <div id="celotor2" style="" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <img style="width: 100%;" src="{{asset('img/Celotor%20Paso%201.jpg')}}" alt="Pasos celotor">
              <div class="carousel-caption">
              </div>
            </div>
            
            <div class="item">
              <img style="width: 100%;" src="{{asset('img/Celotor%20Paso%202.jpg')}}" alt="Pasos celotor">
              <div class="carousel-caption">
              </div>
            </div>
            
            <div class="item">
              <img style="width: 100%;" src="{{asset('img/Celotor%20Paso%203.jpg')}}" alt="Pasos celotor">
              <div class="carousel-caption">
              </div>
            </div>
            
            <div class="item">
              <img style="width: 100%;" src="{{asset('img/Celotor%20Paso%204.jpg')}}" alt="Pasos celotor">
              <div class="carousel-caption">
              </div>
            </div>
          </div>

          <!-- Controls -->
          <a class="left carousel-control" href="#celotor2" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#celotor2" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
    </div>
</div>
                <!--xv-best-seller-->
            </div>  
    <!--Ultimos productos > -->
  
    </div>
    
@endsection

@section('js')
  <script>
   
  </script>
@endsection