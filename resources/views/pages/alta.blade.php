@extends('layouts.template')
@section('titulo', 'Alta Genetics')
@section('contenido')
<div class="container" style="margin-top: 2em;margin-bottom: 2em;">
  <div class="col-md-6">
    <a href="{{route('alta.seccion', [2, 'Corte_Taurino'])}}"><img class="img-rounded alta" style="margin: 10px; transition: 0.5s;" src="{{asset('img/taurino.jpeg')}}" width="100%" alt=""></a>
  </div>
  <div class="col-md-6">
    <a href="{{route('alta.seccion', [1, 'Corte_Zebu'])}}"><img class="img-rounded alta" style="margin: 10px; transition: 0.5s;" src="{{asset('img/zebu.jpeg')}}" width="100%" alt=""></a>
  </div>
  <div class="col-md-6">
    <a href="{{route('alta.seccion', [4, 'Leche_Importada'])}}"><img class="img-rounded alta" style="margin: 10px; transition: 0.5s;" src="{{asset('img/importada.jpeg')}}" width="100%" alt=""></a>
  </div>
  <div class="col-md-6">
    <a href="{{route('alta.seccion', [3, 'Leche_Nacional'])}}"><img class="img-rounded alta" style="margin: 10px; transition: 0.5s;" src="{{asset('img/nacional.jpeg')}}" width="100%" alt=""></a>
  </div>
</div>
<style>
  img.alta:hover{
    box-shadow: 0px 0px 8px black;
    transition: 0.5s;
  }
</style>
@endsection