@extends('layouts.template')
@section('titulo', 'Politicas de privaciad')
@section('contenido')
<div class="container">
    <div class="card">
      <div class="container-fliud">
        <div class="wrapper row">
          <div class="preview col-md-6">
            <img src="{{asset($img)}}" alt="">            
          </div>
          <div class="details col-md-6">
            <h2 class="product-title">{{$item->nombre}}</h2>
            
            <!-- Para el codigo del todo -->
            <!--
            <div class="alert alert-info">
              Codigo del toro: %$5478$%
            </div> -->
            <span>
            <h3><b>CÓDIGO DEL TORO:</b> #{{$item->codigo}} <br><br></h3>
            <a href="{{$item->url}}" class="btn btn-success">Adquirir</a>
            </span></h4>
            {{-- <div class="action">
              <button class="add-to-cart btn btn-default" type="button">Comprar</button>
              <button class="like btn btn-default" type="button"><span class="fa fa-heart"></span></button>
            </div> --}}
          </div>
        </div>
      </div>
    </div>
</div>
<div class="container">
  <div class="tabs-pane mt-60">
        <ul class="tab-sections">
            <li class="active"><a href="#tab01" class="btn-cart">Descripción</a></li>
        </ul>
        <div class="tab-panels">
            <div id="tab01" class="tab-content active">
                {!!$item->descripcion!!}
            </div><!--tab content-->
        </div>
    </div><!--tabs pane-->
</div>
@endsection