<!DOCTYPE html>
<html>
<head>
  <!-- Standard Meta -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

  <!-- Site Properties -->
  <title>Login Example - Semantic</title>
  {!!Html::style('files/components/reset.css')!!}
  {!!Html::style('files/components/site.css')!!}
  
  {!!Html::style('files/components/container.css')!!}
  {!!Html::style('files/components/grid.css')!!}
  {!!Html::style('files/components/header.css')!!}
  {!!Html::style('files/components/image.css')!!}
  {!!Html::style('files/components/menu.css')!!}
  
  {!!Html::style('files/components/divider.css')!!}
  {!!Html::style('files/components/segment.css')!!}
  {!!Html::style('files/components/form.css')!!}
  {!!Html::style('files/components/input.css')!!}
  {!!Html::style('files/components/button.css')!!}
  {!!Html::style('files/components/list.css')!!}
  {!!Html::style('files/components/message.css')!!}
  {!!Html::style('files/components/icon.css')!!}
  
  {!!Html::script('js/jquery.min.js')!!}
  {!!Html::script('files/components/form.js')!!}
  {!!Html::script('files/components/transition.js')!!}

  <style type="text/css">
    body {
      background-color: #DADADA;
    }
    body > .grid {
      height: 100%;
    }
    .image {
      margin-top: -100px;
    }
    .column {
      max-width: 450px;
    }
  </style>
  <script>
  $(document)
    .ready(function() {
      $('.ui.form')
        .form({
          fields: {
            email: {
              identifier  : 'email',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Por favor ingresa tu correo'
                },
                {
                  type   : 'email',
                  prompt : 'Por favor ingresa un correo valido'
                }
              ]
            },
            password: {
              identifier  : 'password',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Insegre una contraseña'
                },
                {
                  type   : 'length[6]',
                  prompt : 'Tu contraseña debe tener como mínimo 6 caracteres'
                }
              ]
            }
          }
        })
      ;
    })
  ;
  </script>
</head>
<body>

<div class="ui middle aligned center aligned grid">
  <div class="column">
   <img width="200px" src="{!!asset('img/logo.PNG')!!}" class="image">
    <h2 class="ui teal image header">
      
      <div class="content">
        Panel de administración - Inicio de sesion
      </div>
    </h2>
    {!!Form::open(['route'=>'log.store', 'method'=>'POST', 'class'=>'ui large form'])!!}
      <div class="ui stacked segment">
        <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
            <input type="text" name="email" placeholder="dirección E-mail">
          </div>
        </div>
        <div class="field">
          <div class="ui left icon input">
            <i class="lock icon"></i>
            <input type="password" name="password" placeholder="Contraseña">
          </div>
        </div>
        <div class="ui fluid large teal submit button">Login</div>
      </div>

      <div class="ui error message"></div>

    {!!Form::close()!!}
    {!!Form::open(['route'=>'registrar.store', 'method'=>'POST', 'class'=>'ui large form'])!!}
      <div class="ui stacked segment">
       <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
            <input type="text" name="name" placeholder="Nombre y apellido">
          </div>
        </div>
        <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
            <input type="text" name="email" placeholder="dirección E-mail">
          </div>
        </div>
        <div class="field">
          <div class="ui left icon input">
            <i class="lock icon"></i>
            <input type="password" name="password" placeholder="Contraseña">
          </div>
        </div>
        <div class="ui fluid large teal submit button">Login</div>
      </div>

      <div class="ui error message"></div>

    {!!Form::close()!!}

  </div>
</div>

</body>

</html>