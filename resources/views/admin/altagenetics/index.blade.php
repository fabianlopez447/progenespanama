@extends('layouts.adminTemplate')

@section('contenido')

	@include('alerts.success')
	@include('alerts.destroy')

<div class="ui segment center aligned">
	<table class="ui compact inverted celled definition table">
  <thead class="full-width">
    <tr>
      <th>IMAGEN</th>
      <th>NOMBRE</th>
      <th>SUB CATEGORÍA</th>
      <th>CODIGO TORO</th>
      <th>OPCIÓN</th>
    </tr>
  </thead>
  <tbody>
  @foreach($product as $products)
   <?php $cate = \App\Altasubcat::where('id', $products->categoria)->first(); ?>
    <tr style="text-transform: uppercase;">
      <td class="collapsing">
          <img width="80px" src="{!!asset('altagenetics/'.$products->imagen)!!}" alt=""> <label></label>
      </td>
      <td>{!! $products->nombre!!}</td>
      <td>{!! $cate->nombre!!}</td>
      <td>{!! $products->codigo!!}</td>
      <td><a class="ui inverted button yellow icon floated" href="{!!asset('admin/altagenetics/'.$products->id.'/edit')!!}"><i class="ui icon edit"></i></a>
      	<a href="{{ route('admin.altagenetics.destroy',$products->id)}}" class="ui inverted button red icon floated"><i class="ui icon trash outline"></i></a> 
      	<a class="ui inverted button green icon floated" href=""><i class="ui icon unhide"></i></a>
      
      </td>
    </tr>
    @endforeach
  </tbody>
  <tfoot class="full-width">
    <tr>
      <th colspan="4">
      <a class="ui left floated large inverted blue button" href="{!!route('admin.altagenetics.create')!!}">Agregar producto</a>
      <a class="ui left floated large inverted blue button" href="{!!route('admin.altacat.index')!!}">Agregar categoría</a>
      <a class="ui left floated large inverted blue button" href="{!!route('admin.altasubcat.index')!!}">Agregar Subcategoría</a>
      </th>
      <th colspan="2">
          {!!Form::open(['route'=>'admin.descuentos.show', 'method'=>'get', 'class'=>'ui search left aligned'])!!}
			  <div class="ui icon input">
				{!!Form::hidden('resulset', null,['class'=>'prompt', 'placeholder'=>'Buscar producto'])!!}
				
			  </div>
			{!!Form::close()!!}
      </th>
    </tr>
  </tfoot>
</table>
</div>

@endsection