@extends('layouts.adminTemplate')

@section('contenido')
<br>
@if(count($errors)> 0)
<div class="ui inverted red segment">
  <ul>ERROR<hr>
  	 @foreach($errors->all() as $error)
  	 			
  	 			<li>{{$error}}</li>
  	 @endforeach
  </ul>
</div>
@endif

<div class="ui teal segments">
  <div class="ui inverted segment">
      <div class="ui segment" >
    {!!Form::open(['route'=>'admin.altagenetics.store','method'=>'POST', 'class'=>'ui form ingresar', 'files' => true, 'enctype'=>'multipart/form-data'])!!}
      <h2 class="ui dividing header">Información del producto</h2>
      <div class="fields">
        <div class="eight wide field">
          <label>Nombre</label>
          {!!Form::text('nombre', null,['placeholder'=>'Nombre del  producto...'])!!}
        </div>
        <div class="eight wide field">
          <label>Imagen</label>
            <div class="field">
              <input type="file" name="imagen">
          </div>
        </div>
         <div class="eight wide field">
         <label>Categoría</label>
          <select name="categoria">
             <option disabled>►Corte Zebu</option>
               @foreach($cat as $cats)
               @if($cats->idcat == 1)
               <option disabled>∟{{$cats->nombre}}</option>
                 @foreach($subcat as $subcats)
                   @if($subcats->idcat == $cats->id)
                     <option value="{{$subcats->id}}" >∟{{$subcats->nombre}}</option>
                   @endif
                 @endforeach
               @endif
               @endforeach
             <option disabled>►Corte Taurino</option>
              @foreach($cat as $cats)
                @if($cats->idcat == 2)
                 <option disabled>∟{{$cats->nombre}}</option>
               @foreach($subcat as $subcats)
                   @if($subcats->idcat == $cats->id)
                     <option value="{{$subcats->id}}"> ∟{{$subcats->nombre}}</option>
                   @endif
                 @endforeach
                @endif
              @endforeach
             <option disabled>►Leche Nacional</option>
              @foreach($cat as $cats)
               @if($cats->idcat == 3)
                  <option disabled>∟{{$cats->nombre}}</option>
               @foreach($subcat as $subcats)
                   @if($subcats->idcat == $cats->id)
                     <option value="{{$subcats->id}}"> ∟{{$subcats->nombre}}</option>
                   @endif
                 @endforeach
               @endif
              @endforeach
             <option disabled>►Leche Importada</option>
              @foreach($cat as $cats)
              @if($cats->idcat == 4)
                 <option disabled>∟{{$cats->nombre}}</option>
                @foreach($subcat as $subcats)
                   @if($subcats->idcat == $cats->id)
                     <option value="{{$subcats->id}}"> ∟{{$subcats->nombre}}</option>
                   @endif
                 @endforeach
                 @endif
               @endforeach
            </select>
        </div>
        <div class="eight wide field">
          <label>Codigo</label>
          {!!Form::text('codigo', null,['placeholder'=>'Codigo del toro'])!!}
        </div>
      </div>
      <div class="two fields">
        <div class="field">
          <label>Descripción</label>
          {!!Form::textarea('descripcion', null,['placeholder'=>'Descripción del descuento...', 'id'=>'descripcion'])!!}
        </div>
        
         <div class="eight wide field">
          <label>URL</label>
          {!!Form::text('url', null,['placeholder'=>'URL'])!!}
        </div>
      </div>
      
   
     
      <button class="ui primary submit button">Aceptar</button>
    {!!Form::open()!!}
  </div>
  </div>
</div>

@endsection

@section('js')
<script src="//cdn.ckeditor.com/4.6.0/standard/ckeditor.js"></script>
<script type="text/javascript">
CKEDITOR.replace("descripcion");
</script>
@endsection