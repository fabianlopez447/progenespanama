@extends('layouts.adminTemplate')

@section('contenido')

	@include('alerts.success')
	@include('alerts.destroy')

<div class="ui segment center aligned">
	<table class="ui compact inverted celled definition table">
  <thead class="full-width">
    <tr>
      <th>IMAGEN</th>
      <th>NOMBRE</th>
      <th>DESCRIPCION.</th>
      <th>PRECIO</th>
      <th>OPCIÓN</th>
    </tr>
  </thead>
  <tbody>
  @foreach($product as $products)
    <tr style="text-transform: uppercase;">
      <td class="collapsing">
          <img width="64px" src="{!!asset('bmv/'.$products->imagen)!!}" alt=""> <label></label>
     
      </td>
      
      <td>{!! $products->nombre!!}</td>
      <td>{!!$products->descripcion!!}</td>
      <td>{!!$products->precio!!}</td>
      <td><a class="ui inverted button yellow icon floated" href="{!!asset('admin/bmv/'.$products->id.'/edit')!!}"><i class="ui icon edit"></i></a>
      	<a href="{{ route('admin.bmv.destroy',$products->id)}}" class="ui inverted button red icon floated"><i class="ui icon trash outline"></i></a> 
      	<a class="ui inverted button green icon floated" href=""><i class="ui icon unhide"></i></a>
      
      </td>
    </tr>
    @endforeach
  </tbody>
  <tfoot class="full-width">
    <tr>
      <th colspan="4">
      <a class="ui left floated large inverted blue button" href="{!!route('admin.bmv.create')!!}">Agregar producto - BMV Technology</a>
      </th>
      <th colspan="2">
          {!!Form::open(['route'=>'admin.descuentos.show', 'method'=>'get', 'class'=>'ui search left aligned'])!!}
			  <div class="ui icon input">
				{!!Form::text('resulset', null,['class'=>'prompt', 'placeholder'=>'Buscar producto'])!!}
				<i class="search icon"></i>
			  </div>
			{!!Form::close()!!}
      </th>
    </tr>
  </tfoot>
</table>
</div>

@endsection