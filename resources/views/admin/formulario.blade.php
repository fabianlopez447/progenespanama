@extends('layouts.adminTemplate')
@section('contenido')
{!!Form::open(['route'=>'registrar.store', 'method'=>'POST', 'class'=>'ui large form'])!!}
      <div class="ui stacked segment">
       <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
            <input type="text" name="name" placeholder="Nombre y apellido">
          </div>
        </div>
        <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
            <input type="text" name="email" placeholder="dirección E-mail">
          </div>
        </div>
        <div class="field">
          <div class="ui left icon input">
            <i class="lock icon"></i>
            <input type="password" name="password" placeholder="Contraseña">
          </div>
        </div>
        <div class="ui fluid large teal submit button">Login</div>
      </div>

      <div class="ui error message"></div>

    {!!Form::close()!!}
@endsection