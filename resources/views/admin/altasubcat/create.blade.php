@extends('layouts.adminTemplate')

@section('contenido')
<br>
@if(count($errors)> 0)
<div class="ui inverted red segment">
  <ul>ERROR<hr>
  	 @foreach($errors->all() as $error)
  	 			
  	 			<li>{{$error}}</li>
  	 @endforeach
  </ul>
</div>
@endif

<div class="ui teal segments">
  <div class="ui inverted segment">
      <div class="ui segment" >
    {!!Form::open(['route'=>'admin.altasubcat.store','method'=>'POST', 'class'=>'ui form ingresar', 'files' => true, 'enctype'=>'multipart/form-data'])!!}
      <h2 class="ui dividing header">Agregar nueva Sub-categorias - Alta Genetics</h2>
      <div class="fields">
        <div class="eight wide field">
          <label>Nombre</label>
          {!!Form::text('nombre', null,['placeholder'=>'Nombre'])!!}
        </div>
        <div class="eight wide field">
        <label>Categoría</label>

         <select name="idcat">
             <option disabled>Corte Zebu</option>
               @foreach($cat as $cats)
               @if($cats->idcat == 1)
               <option value="{{$cats->id}}"> - {{$cats->nombre}}</option>
               @endif
               @endforeach
             <option disabled>Corte Taurino</option>
              @foreach($cat as $cats)
                @if($cats->idcat == 2)
               <option value="{{$cats->id}}"> - {{$cats->nombre}}</option>
                @endif
              @endforeach
             <option disabled>Leche Nacional</option>
              @foreach($cat as $cats)
               @if($cats->idcat == 3)
               <option value="{{$cats->id}}"> - {{$cats->nombre}}</option>
               @endif
              @endforeach
             <option disabled>Leche Importada</option>
              @foreach($cat as $cats)
              @if($cats->idcat == 4)
               <option value="{{$cats->id}}"> - {{$cats->nombre}}</option>
                 @endif
               @endforeach
            </select>
        </div>
      </div>
  
     
      <button class="ui primary submit button">Aceptar</button>
    {!!Form::open()!!}
  </div>
  </div>
</div>

@endsection

@section('js')

@endsection