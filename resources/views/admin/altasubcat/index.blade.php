@extends('layouts.adminTemplate')

@section('contenido')

	@include('alerts.success')
	@include('alerts.destroy')

<div class="ui segment center aligned">
	<table class="ui compact inverted celled definition table">
  <thead class="full-width">
    <tr>
 
      <th>NOMBRE</th>
      <th>SECCIÓN</th>
      <th>CATEGORÍA</th>
      <th>OPCIÓN</th>
    </tr>
  </thead>
  <tbody>
  @foreach($product as $products)
    <tr style="text-transform: uppercase;">
      <td>{!! $products->nombre!!}</td>
      <?php $cate = \App\Altacat::where('id', $products->idcat)->first(); ?>
      <td>
      @if($cate->idcat == 1)
      Corte Zebu
      @elseif($cate->idcat == 2)
      Corte Taurino
      @elseif($cate->idcat == 3)
      Leche Nacional
      @else
      Leche Importada
      @endif
      </td>
       <td>
          {{$cate->nombre}}
      </td>
     
      <td>
      	<a href="{{ route('admin.altasubcat.destroy',$products->id)}}" class="ui inverted button red icon floated"><i class="ui icon trash outline"></i></a> 
  
      
      </td>
    </tr>
    @endforeach
  </tbody>
  <tfoot class="full-width">
    <tr>
      <th colspan="4">
      <a class="ui left floated large inverted blue button" href="{!!route('admin.altasubcat.create')!!}">Agregar Sub-categoría </a>
      </th>
     
    </tr>
  </tfoot>
</table>
</div>

@endsection