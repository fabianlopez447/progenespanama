@extends('layouts.adminTemplate')
@section('contenido')
	<style>
		#addMore{
			margin-top: 150px;
			font-size: 32px;
			margin-bottom: 150px;
		}
		#addMore{
			color: aqua;
		}
	</style>
	<h1 class="ui header">Cambiar imagen del carrusel.</h1>
	<h3 class="ui header">Seleccione la imagen y de click en "Cambiar"</h3>
	<div class="ui grid container center floated">
		<div class="four column row">
		<?php $n = 1; ?>
		@foreach($slider as $Carrusel)
			<div class="four wide column">
				<div class="ui card">
					<div class="content">
						<h4 class="ui header blue">Imagen <?php echo $n; $n++; ?></h4>
					</div>
					<div  class="image"><img width="100%" src="{!!asset('carrusel/'.$Carrusel->imagen)!!}" alt=""></div>
					<div class="content">
						{!!Form::model($Carrusel,['route'=>['admin.carrusel.update', $Carrusel->id], 'method'=>'PUT', 'class'=>'ui form', 'files' => true, 'enctype'=>'multipart/form-data'])!!}
							<div class="field">
								<label for="Imagen">Inserte imagen</label>
								{!!Form::file('imagen')!!}
							</div>
							<div class="field">
								<label for="Codigo">Direccion URL(Link)</label>
								{!!Form::text('codigo_descuento', null)!!}
							</div>
							{!!Form::hidden('image_old', $Carrusel->imagen)!!}
							<div class="field">
								<a href="{{route('admin.carrusel.destroy', $Carrusel->id)}}" class="ui button red">Quitar</a>
								<button type="submit" class="ui button orange">Cambiar</button>
							</div>
						{!!Form::close()!!}
					</div>
				</div>
			</div>
		@endforeach
		<!-- Para añadir -->
			<div class="four wide column">
				<a href="{!!route('admin.carrusel.create')!!}" id="addMore" title="Añadir uno" class="ui center aligned icon header grey">
				  <i class="plus icon"></i>
				</a>
			</div>
		</div>
	</div>
@endsection