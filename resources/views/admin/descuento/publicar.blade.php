@extends('layouts.adminTemplate')

@section('contenido')
<br>
@if(count($errors)> 0)
<div class="ui inverted red segment">
  <ul>ERROR<hr>
  	 @foreach($errors->all() as $error)
  	 			
  	 			<li>{{$error}}</li>
  	 @endforeach
  </ul>
</div>
@endif

<div class="ui teal segments">
  <div class="ui inverted segment">
      <div class="ui segment" >
    {!!Form::open(['route'=>'admin.descuentos.store','method'=>'POST', 'class'=>'ui form ingresar', 'files' => true, 'enctype'=>'multipart/form-data'])!!}
      <h2 class="ui dividing header">Información del descuento</h2>
      <div class="fields">
        <div class="eight wide field">
          <label>Nombre</label>
          {!!Form::text('nombre', null,['placeholder'=>'Nombre del descuento...'])!!}
        </div>
        <div class="eight wide field">
          <label>imagen</label>
            <div class="field">
              <input type="file" name="imagen">
          </div>
        </div>
      </div>
      <div class="two fields">
        <div class="field">
          <label>Descripción</label>
          {!!Form::textarea('descripcion', null,['placeholder'=>'Descripción del descuento...', 'id'=>'descripcion'])!!}
        </div>
        <div class="field">
          <label>Restricción</label>
            {!!Form::textarea('restriccion', null,['placeholder'=>'Restricción del descuento...', 'id'=>'restriccion'])!!}
        </div>
      </div>
       <div class="two fields">
        <div class="field">
          <label>Porcentaje de descuento</label>
          {!!Form::number('descuento', null,['placeholder'=>'50%'])!!}
        </div>
        <div class="field">
          <label>Fecha de vencimiento</label>
            {!!Form::date('fecha_vencimiento', \Carbon\Carbon::now())!!}
        </div>
      </div>
      <h2 class="ui dividing header">Información de la empresa</h2>
      <div class="field">
        <label>Descripción de la empresa</label>
          {!!Form::textarea('empresa', null,['id'=>'empresa'])!!}
      </div>
      <div class="fields">
        <div class="six wide field">
          <label> Facebook </label>
          {!!Form::text('red_facebook', null,['placeholder'=>'http://facebook.com/...'])!!}
        </div>
        <div class="six wide field">
          <label>Instagram</label>
          {!!Form::text('red_instagram', null,['placeholder'=>'http://instagram.com/...'])!!}
        </div>
        <div class="six wide field">
          <label>Twitter</label>
          {!!Form::text('red_twitter', null,['placeholder'=>'http://twitter.com/...'])!!}
        </div>
      </div>
      <div class="fields">
       <div class="six wide field">
          <label>Email</label>
          {!!Form::email('empresa_email', null,['placeholder'=>'correo@mail.co...'])!!}
        </div>
        <div class="three wide field">
          <label>Ciudad</label>
          {!!Form::text('ciudad', null,['placeholder'=>'Direccion'])!!}
        </div>
        <div class="three wide field">
          <label>Direccion</label>
          {!!Form::text('direccion', null,['placeholder'=>'Direccion'])!!}
        </div>
        <div class="six wide field">
          <label> telefono </label>
          {!!Form::text('telefono', null,['placeholder'=>'número telefonico'])!!}
        </div>
      </div>
      <div class="two fields">
        <div class="field eight wide">
          <label>Insertar vídeo</label>
          {!!Form::text('video', null,['placeholder'=>'http://superdescuentospanama.com...'])!!}
        </div>
      
        <div class="field eight wide">
          <label>Insertar mapa</label>
          {!!Form::text('coordenadas', null,['placeholder'=>'iframe src=https://www.google.com/...'])!!}
        </div>
      </div>
       <h2 class="ui dividing header">Carnet</h2>
        <div class="fields">
        <div class="eight wide field">
          <label>Codigo del carnet</label>
          {!!Form::text('carnet_codigo', null,['placeholder'=>'Nombre del descuento...'])!!}
        </div>
        <div class="eight wide field">
          <label>Imagen del carnet</label>
            <div class="field">
              {!!Form::file('carnet_imagen', null)!!}
          </div>
        </div>
      </div>
      <div class="fields">
        <div class="eight wide field">
          <label>Categoría</label>
          {!!Form::select('categoria', [
          '1' => 'Articulos Exclusivos',
          '2' => 'Belleza y Estetica',
          '3' => 'Entretenimiento',
          '4' => 'Hoteles y Turismo',
          '5' => 'Mascotas',
          '6' => 'Niños y Bebes',
          '7' => 'Restaurantes y Cafeterías',
          '8' => 'Salud',
          '9' => 'Servicios Automotrices',
          '10' => 'Servicios Corporativos',
          '11' => 'Tiendas'
          ])!!}
        </div>
      </div>
      <button class="ui primary submit button">Aceptar</button>
    {!!Form::open()!!}
  </div>
  </div>
</div>

@endsection

@section('js')
<script src="//cdn.ckeditor.com/4.6.0/standard/ckeditor.js"></script>
<script type="text/javascript">
CKEDITOR.replace("descripcion");
CKEDITOR.replace("restriccion");
CKEDITOR.replace("empresa");
</script>
@endsection