@extends('layouts.adminTemplate')

@section('contenido')

	@include('alerts.success')
	@include('alerts.destroy')

<div class="ui segment center aligned">
	<table class="ui compact inverted celled definition table">
  <thead class="full-width">
    <tr>
      <th></th>
      <th>TITULO</th>
      <th>DESC.</th>
      <th>CARNET</th>
      <th>URL</th>
      <th>OPCIÓN</th>
    </tr>
  </thead>
  <tbody>
  	@foreach($descuento as $Descuento)
    <tr style="text-transform: uppercase;">
      <td class="collapsing">
          <img width="64px" src="{!!asset('descuentos/'.$Descuento->imagen)!!}" alt=""> <label></label>
     
      </td>
      
      <td>{!!$Descuento->nombre!!}</td>
      <td><h2 class="ui header red">{!!$Descuento->descuento!!}%</h2></td>
      <td>{!!$Descuento->carnet_codigo!!}</td>
      <td>{!!$Descuento->sitio_url!!}</td>
      <td><a class="ui inverted button yellow icon floated" href="{!!asset('admin/descuentos/'.$Descuento->id.'/edit')!!}"><i class="ui icon edit"></i></a>
      <a href="{{ route('admin.descuentos.destroy',$Descuento->id)}}" class="ui inverted button red icon floated"><i class="ui icon trash outline"></i></a> 
      <a class="ui inverted button green icon floated" href="{!!route('descuento.detalles', $Descuento->id)!!}"><i class="ui icon unhide"></i></a>
      	<a class="ui inverted button blue floated" href="{!!asset('admin/semanal/'.$Descuento->id.'/add')!!}">Descuento de la semana</a>
		<a class="ui inverted button green floated" href="{!!asset('admin/mensual/'.$Descuento->id.'/add')!!}">Descuento del mes</a>
      </td>
    </tr>
    @endforeach
  </tbody>
  <tfoot class="full-width">
    <tr>
      <th colspan="5">
      <a class="ui left floated large inverted blue button" href="{!!route('admin.descuentos.create')!!}">Agregar descuento</a>
      </th>
      <th colspan="2">
          {!!Form::open(['route'=>'admin.descuentos.show', 'method'=>'get', 'class'=>'ui search left floated'])!!}
			  <div class="ui icon input">
				{!!Form::text('resulset', null,['class'=>'prompt left floated', 'placeholder'=>'Buscar descuento'])!!}
				<i class="search icon"></i>
			  </div>
			{!!Form::close()!!}
      </th>
    </tr>
  </tfoot>
</table>
</div>

@endsection