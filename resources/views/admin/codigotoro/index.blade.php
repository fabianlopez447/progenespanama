@extends('layouts.adminTemplate')

@section('contenido')

	@include('alerts.success')
	@include('alerts.destroy')

<div class="ui segment center aligned">
	<table class="ui compact inverted celled definition table">
  <thead class="full-width">
    <tr>
      <th>NOMBRE</th>
      <th>DESCRIPCION.</th>
      <th>URL</th>
      <th>OPCIÓN</th>
    </tr>
  </thead>
  <tbody>
  @foreach($product as $products) 
      <td>{!! $products->nombre!!}</td>
      <td>{!!$products->codigo!!}</td>
      <td>{!!$products->url!!}</td>
      <td><a class="ui inverted button yellow icon floated" href="{!!asset('admin/codigotoro/'.$products->id.'/edit')!!}"><i class="ui icon edit"></i></a>
      	<a href="{{ route('admin.codigotoro.destroy',$products->id)}}" class="ui inverted button red icon floated"><i class="ui icon trash outline"></i></a> 
      
      </td>
    </tr>
    @endforeach
  </tbody>
  <tfoot class="full-width">
    <tr>
      <th colspan="4">
      <a class="ui left floated large inverted blue button" href="{!!route('admin.codigotoro.create')!!}">Agregar producto</a>
      </th>
      
    </tr>
  </tfoot>
</table>
</div>

@endsection