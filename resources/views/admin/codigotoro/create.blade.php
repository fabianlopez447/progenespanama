@extends('layouts.adminTemplate')

@section('contenido')
<br>
@if(count($errors)> 0)
<div class="ui inverted red segment">
  <ul>ERROR<hr>
  	 @foreach($errors->all() as $error)
  	 			
  	 			<li>{{$error}}</li>
  	 @endforeach
  </ul>
</div>
@endif

<div class="ui teal segments">
  <div class="ui inverted segment">
      <div class="ui segment" >
    {!!Form::open(['route'=>'admin.codigotoro.store','method'=>'POST', 'class'=>'ui form ingresar', 'files' => true, 'enctype'=>'multipart/form-data'])!!}
      <h2 class="ui dividing header">Agregar nuevo toro al buscador.</h2>
      <div class="fields">
        <div class="eight wide field">
          <label>Nombre</label>
          {!!Form::text('nombre', null,['placeholder'=>'Nombre del toro'])!!}
        </div>
         <div class="eight wide field">
          <label>Codigo</label>
          {!!Form::text('codigo', null,['placeholder'=>'Codigo del toro'])!!}
        </div>
         <div class="eight wide field">
          <label>URL</label>
          {!!Form::text('url', null,['placeholder'=>'URL'])!!}
        </div>
      
      </div>
     
      
   
     
      <button class="ui primary submit button">Aceptar</button>
    {!!Form::open()!!}
  </div>
  </div>
</div>

@endsection

@section('js')
<script src="//cdn.ckeditor.com/4.6.0/standard/ckeditor.js"></script>
<script type="text/javascript">
CKEDITOR.replace("descripcion");
</script>
@endsection