@extends('layouts.adminTemplate')

@section('contenido')
@include('alerts.destroy')

<div class="ui segment center aligned">
	<table class="ui compact inverted celled definition table">
  <thead class="full-width">
    <tr>
      <th>NOMBRE</th>
      <th>APELLIDO</th>
      <th>EMAIL</th>
      <th>FAVORITO</th>
      <th>OPCIÓN</th>
    </tr>
  </thead>
  <tbody>
  	@foreach($suscriptor as $Suscriptor)
    <tr style="text-transform: uppercase;">
      <td>{!!$Suscriptor->nombre!!}</td>
      <td><h2 class="ui header red">{!!$Suscriptor->apellido!!}</h2></td>
      <td>{!!$Suscriptor->email!!}</td>
      <td>
        @if($Suscriptor->favorito == 1)
          <i  class="icon diamond"></i>Articulos Exclusivos
        @endif
        @if($Suscriptor->favorito == 2)
          <i class="icon eye"></i>Belleza y Estetica
        @endif
        @if($Suscriptor->favorito == 3)
          <i class="icon puzzle"></i>Entretenimiento
        @endif
        @if($Suscriptor->favorito == 4)
          <i class="icon hotel"></i>Hoteles y Turismo
        @endif
        @if($Suscriptor->favorito == 5)
          <i class="icon paw"></i>Mascotas
        @endif
        @if($Suscriptor->favorito == 6)
          <i class="icon child"></i>Niños y Bebes
        @endif
        @if($Suscriptor->favorito == 7)
          <i class="icon food"></i>Restaurantes y Cafeterías
        @endif
        @if($Suscriptor->favorito == 8)
          <i class="icon heartbeat"></i>Salud
        @endif
        @if($Suscriptor->favorito == 9)
          <i class="icon car"></i>Servicios Automotrices
        @endif
        @if($Suscriptor->favorito == 10)
          <i class="icon building outline"></i>Servicios Corporativos
        @endif
        @if($Suscriptor->favorito == 11)
          <i class="icon shopping bag"></i>Tiendas
        @endif
      </td>
      <td>
      <a href="{{ route('admin.suscriptor.destroy',$Suscriptor->id)}}" class="ui inverted button red icon floated"><i class="ui icon trash outline"></i></a> </td>
    </tr>
    @endforeach
  </tbody>
    <tfoot class="full-width">
    <tr>
      <th colspan="5">
      <a class="ui left floated large inverted blue button" href="{!!route('admin.descuentos.create')!!}">Agregar descuento</a>
      </th>
      <th colspan="2">
         
			  <div class="ui icon input">
				{!!Form::text('resulset', null,['class'=>'prompt left floated', 'placeholder'=>'Buscar descuento'])!!}
				<i class="search icon"></i>
			  </div>
			{!!Form::close()!!}
      </th>
    </tr>
  </tfoot>
</table>
</div>


@endsection