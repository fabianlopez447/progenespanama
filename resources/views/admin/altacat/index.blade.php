@extends('layouts.adminTemplate')

@section('contenido')

	@include('alerts.success')
	@include('alerts.destroy')

<div class="ui segment center aligned">
	<table class="ui compact inverted celled definition table">
  <thead class="full-width">
    <tr>
 
      <th>NOMBRE</th>
      <th>Sección</th>

      <th>OPCIÓN</th>
    </tr>
  </thead>
  <tbody>
  @foreach($product as $products)
    <tr style="text-transform: uppercase;">
      <td>{!! $products->nombre!!}</td>
      <td>
      @if($products->idcat == 1)
      Corte Zebu
      @elseif($products->idcat == 2)
      Corte Taurino
      @elseif($products->idcat == 3)
      Leche Nacional
      @else
      Leche Importada
      @endif
      </td>
     
      <td>
      	<a href="{{ route('admin.progenes.destroy',$products->id)}}" class="ui inverted button red icon floated"><i class="ui icon trash outline"></i></a> 
  
      
      </td>
    </tr>
    @endforeach
  </tbody>
  <tfoot class="full-width">
    <tr>
      <th colspan="4">
      <a class="ui left floated large inverted blue button" href="{!!route('admin.altacat.create')!!}">Agregar categoría </a>
      </th>
     
    </tr>
  </tfoot>
</table>
</div>

@endsection