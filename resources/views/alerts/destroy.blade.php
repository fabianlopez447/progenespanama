@if(Session::has('destroy'))

	<div class="ui modal tiny">
		<div class="image content center aligned">
	      	<img style="width: 64px; height: 64px;" class="image" src="{!!asset('img/destroy.png')!!}">
	      	<div class="description">
	      		<p class="ui header">{{Session::get('destroy')}}</p>
	    	</div>
	  	</div>
	  	<div class="actions">
		    <div class="ui approve button">Aceptar</div>
		 </div>
	</div>

	  <script>
		$('.ui.modal')
		  .modal()
		  .modal('show')

		;
	  </script>
@endif

