<!DOCTYPE html>
<!--[if lte IE 9]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<title>@yield('titulo') - Progenes Panamá</title>
<!--=================================
Meta tags
=================================-->
<meta name="description" content="">
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta name="viewport" content="minimum-scale=1.0, width=device-width, maximum-scale=1, user-scalable=no" />
<!--=================================
Style Sheets
=================================-->
@yield('style')
<link href="https://fonts.googleapis.com/css?family=Courgette" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,600' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=Asap" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
{!!Html::style('assets/css/bootstrap.min.css')!!}
{!!Html::style('css/product.css')!!}
{!!Html::style('css/panel.css')!!}
{!!Html::style('assets/css/font-awesome.min.css')!!}
{!!Html::style('assets/css/owl.carousel.css')!!}
{!!Html::style('assets/css/flags.css')!!}
{!!Html::style('assets/css/main.css')!!}
{!!Html::style('css/style.css')!!}


</head>
<body style="background: #E6E6E6;">

<!--========================================
Header
===========================================--> 
<header class="doc-header">
    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-7">
                    <ul class="top-nav">
                        <li><a style="font-size: 20px; color: gold;font-family: 'Courgette', cursive;">Valorando el éxito a través de la confianza</a></li>

                    </ul>
                </div>
                <div class="col-xs-12 col-md-5">
                    {{-- <ul class="top-nav text-right">
                        <li><a href="#"><i class="fa fa-user"></i>Ingresar</a></li>
                        <li><a href="#"><i class="fa fa-lock"></i>Registrarse</a></li>
                    </ul> --}}
                </div>
            </div>
        </div>
    </div><!--top header-->
    <div class="header-bottom">
        <div class="container">
            <a class="logo" href="/" style="max-width: 25%">
                <img src="{{asset('assets/img/logo.png')}}"  alt=""/>
            </a>
            <div class="clearfix"></div>
           <nav>
                <a class="nav-triger" href="#">
                    <img src="{{asset('assets/img/basic/menu55.png')}}" alt="">
                </a>
                <ul class="main_menu">
                    <li><a href="{!!route('progenescatalogo')!!}">Progenes</a></li>
                    <li><a href="{!!route('AltaGenetics')!!}">Alta Genetics</a></li>
                    <li><a href="{!!route('bmvcatalogo')!!}">BMV Technology</a></li>
                    <li><a href="{!!route('celotor')!!}">Celotor</a></li>
                    <!--<li class="home-dropdown"><a href="#">Nuestra empresa</a>-->
                    <!--
                        <ul>
                            <li><a href="blog-masonry.html">Blog Masonry</a></li>
                            <li><a href="blog1.html">Blog Classic</a></li>
                            <li><a href="blog-detail.html">Blog Single</a></li>
                            <li><a href="detail1.html">Product Detail</a></li>
                            <li><a href="archive.html">Shop Archive</a></li>
                            <li><a href="cart1.html">Shop Cart</a></li>
                            <li><a href="checkout1.html">Shop Checkout</a></li>
                        </ul>
                    -->
                    </li>
                    <!--<li><a href="#">Suscribete</a></li>-->
                   
                </ul>
        </div>
    </div><!--header Bottom-->
</header><!--document Header-->   
<!--========================================
Search
===========================================-->

<div class="social-search style2 style-wide">
    <div class="container">
            <div class="social">
                <span>Bienvenido a Progenes<br>Teléfono: (+800) 5146 - 9000</span>
                <ul>
                    <li><a target="_blank" href="https://www.facebook.com/progenesSA/"><i class="fa fa-facebook"></i></a></li>
                    <li><a target="_blank" href="https://www.instagram.com/progenespanama/"><i class="fa fa-instagram"></i></a></li>
                </ul><!--ul-->
            </div><!--social-->
            <div style="margin-top:10px;" class="search-form">
               
                 <a style="font-size:18px;color:#fff;" href="/contacto">Contactanos</a>
            </div>
    </div><!--container-->
</div><!--social-search-->
<div class="clearfix"></div>   
<!--=======Page Content Area=========-->   
<main> 
    	@yield('contenido')  
</main>
    
    
<!--========================================
Footer
===========================================-->
    
<footer class="doc-footer footer-dark">
    <div class="footer-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="contact">
                    <a class="ft-logo pb-20 img-logo" href="#"><img src="{{asset('assets/img/logo.png')}}" alt=""></a>
                        <ul class="no-preIcon customeIconList pt-30">
                            <li>
                                <i class="fa fa-phone"></i>
                                <a href="#">6934 5534 - 6822 3066</a>
                            </li>
                            <li>
                                <i class="fa fa-envelope"></i>
                                <a href="mailto:hieupv@outlook.com">info.progenes@gmail.com</a>
                            </li>
                            <li>
                                <i class="fa fa-map-marker"></i>
                               Ciudad de Panama - Panama
                            </li>
                        </ul>
                    </div><!--content-->
                </div><!--column-3-->
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <h4 class="no-mar">Información</h4>
                    <ul class="mt-25">
                        <li><a href="{{ route('terminos')}}">Terminos y condiciones</a></li>
                        <li><a href="{{ route('politicas')}}">Politicas de privacidad</a></li>
                        <li><a href="/contacto">¿Quieres dar a conocer <br>tu Marca en Panamá?</a></li>
                        <li><a href="/contacto">Reportar problema</a></li>
                        
                    </ul>
                </div><!--column-3-->
                <div class="col-lg-3 col-md-3 col-sm-6">
                   <h4 class="no-mar">Más</h4>
                    <ul class="mt-25">
                        <li><a href="/contacto">Buzón de sugerencias</a></li>
                        <li><a href="/contacto">Asistencia Técnica</a></li>
                        <li><a href="/contacto">Garantía de productos de <br>BMV Technology</a></li>
                    </ul>
                </div>
                
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <h4 class="no-mar">Páginas</h4>
                    <ul class="mt-25">
                        <li><a href="{{ route('progenescatalogo')}}">Progenes</a></li>
                        <li><a href="{{ route('alta.seccion')}}">Alta Genetics</a></li>
                        <li><a href="{{ route('bmvcatalogo')}}">BMV Technology</a></li>
                        <li><a href="{{ route('celotor')}}">Celotor</a></li>
                    </ul>
                </div><!--column-3-->
                <!--column-3-->
            </div><!--row-->
        </div><!--container-->
    </div><!--footer-content-->
    <div class="copy-rights color-white">
        <div class="container">
            <div class="inner">
               <div class="row">
                   <div class="col-xs-12 col-md-6 col-sm-6 col-xs-12">
                       <span>Copyright &copy; 2017 Progenespanama todos los derechos reservados.</span>
                   </div><!--column-6-->
                   <div class="col-xs-12 col-md-6 col-sm-6 col-xs-12">
                        <ul class="clearfix">
                         <!--   <li><img src="assets/img/apple.png" alt=""></li>
                            <li><img src="assets/img/visa.png" alt=""></li>
                            <li><img src="assets/img/paypal.png" alt=""></li>
                            <li><img src="assets/img/master-card.png" alt=""></li>-->
                        </ul>
                   </div><!--column-6-->
                </div><!--row-->
            </div><!--inner-->
        </div><!--container-->
    </div><!--copy-rights-->
</footer><!--doc-footer-->
 
<!--=================================
Script Source
=================================-->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

{!!Html::script('assets/js/jquery.js')!!}
{!!Html::script('assets/js/ajaxify.min.js')!!}
{!!Html::script('assets/js/jquery.easing-1.3.pack.js')!!}
{!!Html::script('assets/js/jquery.countdown.min.js')!!}
{!!Html::script('assets/js/jquery.waitforimages.js')!!}

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
{!!Html::script('assets/js/owl.carousel.min.js')!!}
{!!Html::script('assets/js/masonry.pkgd.min.js')!!}
{!!Html::script('assets/js/main.js')!!}


@yield('js')

</body>
</html>
