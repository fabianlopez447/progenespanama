<!DOCTYPE html>
<html>
<head>
	<title>Admin</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

	{!!Html::style('files/semantic.min.css')!!}
    
  	<!-- Google Fonts -->
   	<link href="https://fonts.googleapis.com/css?family=Italianno" rel="stylesheet">
   

</head>
<body>
<div class="ui fixed inverted large icon menu">
	<div class="item" style="width: 150px;">
		<a href="{!!route('admin.inicio.index')!!}"><img style="width: 100%;" src="{!!asset('img/logo.PNG')!!}"> </a>
  	</div>
  	<a class="item side">
	    <i class="sidebar icon"></i>
	    MENU
	 </a>
  	<a class="item" href="{!!route('admin.inicio.index')!!}">INICIO</a>
  	<a class="item" href="{!!route('admin.carrusel.index')!!}">CARRUSEL</a>  	
  	<a class="item" href="{!!route('admin.progenes.index')!!}">PROGENES</a>
  	<a class="item" href="{!!route('admin.bmv.index')!!}">BMV TECHNOLOGY</a>
  	<a class="item" href="{!!route('admin.altagenetics.index')!!}">ALTA GENETICS</a>
  	<div class="ui right dropdown item log">
    {!!Auth::user()->name!!}
    <i class="dropdown icon"></i>
    <div class="menu">
      <a href="{!!route('salir')!!}" class="item">Salir</a>
    </div>
  </div>
</div>

<div style="margin-top:82px; background: #ecf0f1; height: auto;" class="ui bottom attached segment pushable">
  <div class="ui labeled left inline vertical sidebar menu">
   		<div class="ui item">
			
			<div class="menu">
			  	<a class="item" href="{!!route('admin.inicio.index')!!}">INICIO</a>
  	<a class="item" href="{!!route('admin.carrusel.index')!!}">CARRUSEL</a>  	
  	<a class="item" href="{!!route('admin.progenes.index')!!}">PROGENES</a>
  	<a class="item" href="{!!route('admin.bmv.index')!!}">BMV TECHNOLOGY</a>
  	<a class="item" href="{!!route('admin.altagenetics.index')!!}">ALTA GENETICS</a>
			</div>
		  </div>
     <a class="item" href="{!!route('admin.suscriptor.index')!!}">
      <i class="users icon"></i>
      Suscriptores
    </a>
 
    </div>
  <div class="pusher">
    <div class="ui basic segment">
    	@yield('contenido')
    
</div>
	</div>
	</div>
    <!-- Scripts -->
    {!!Html::script('js/jquery.min.js')!!}
    {!!Html::script('files/semantic.min.js')!!}
    {!!Html::script('js/admin.js')!!}

    @yield('js')
</body>
</html>