<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Descuento_mensual extends Model
{
    protected $table = 'descuento_mensual';
    protected $fillable = ['nombre','imagen','descripcion','id_descuento','empresa','descuento','red_twitter','red_facebook','red_instagram','empresa_email','coordenadas','carnet_imagen','carnet_codigo','fecha_vencimiento'];
}
