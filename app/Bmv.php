<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bmv extends Model
{
         protected $table = 'bmv';
    protected $fillable = ['nombre','imagen','descripcion','precio'];

}
