<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contactados extends Model
{
     protected $table = 'usuarios_contactados';
    protected $fillable = ['nombre','asunto','email','mensaje'];

}
