<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class productosController extends Controller
{
    public function index()
    {
        $slider = \App\Carrusel::orderBy('created_at', 'desc')->get();
        $progenes = \App\Progenes::orderBy('created_at', 'desc')->paginate(3);
        $alta = \App\Altagenetics::orderBy('created_at', 'desc')->paginate(6);
        $bmv = \App\Bmv::orderBy('created_at', 'desc')->paginate(3);
        return view('pages.index.indexComputer', compact('slider','progenes', 'alta','bmv'));
    }   

    public function alta(){
        return view('pages.alta');
    }
    public function seccion($id, $nombre){
        $nombre = str_replace("_", " ", $nombre);
        $product = \App\Altagenetics::orderBy('created_at', 'desc')->where('seccion', $id)->paginate(10);
        return view('pages.secciones', compact('product', 'nombre'));
    }

    public function listado($id){
        $cate = $id;
        $desc = \App\Descuento::where('categoria', $id)->orderBy('created_at', 'desc')->paginate(10);
        return view('pages.lista', compact('desc','cate'));
    }

    public function detallesProgenes($id){
        $item = \App\Progenes::find($id);
        $img = 'progenes/'.$item->imagen;
        return view('pages.detalle-producto', compact('item', 'img'));
    }
    public function detallesAlta($id){
        $item = \App\Altagenetics::find($id);
        $img = 'altagenetics/'.$item->imagen;
        return view('pages.detalles', compact('item', 'img'));
    }
    public function detallesBmv($id){
        $item = \App\Bmv::find($id);
        $img = 'bmv/'.$item->imagen;
        return view('pages.detalle-producto', compact('item', 'img'));
    }
    
    public function progenescatalogo(){
       $product = \App\Progenes::orderBy('created_at','desc')->paginate(4);
        return view('pages.progenes',compact ('product'));
    }
   
   public function bmvcatalogo(){
       $product = \App\Bmv::orderBy('created_at','desc')->paginate(4);
        return view('pages.bmv',compact ('product'));
    }
   
   public function celotor(){
        return view('pages.celotor');
    }
   
   
    public function marketing(){
        return view('pages.marketing-magic');
    }
    public function politicas(){
        return view('pages.politicas');
    }
    public function terminos(){
        return view('pages.terminos');
    }
   public function tumarca(){
        return view('pages.tu-marca');
    }
   public function reportar(){
        return view('pages.reportar');
    }
   public function sugerencias(){
        return view('pages.sugerencias');
    }
   public function asistencia(){
        return view('pages.asistencias');
    }
   public function garantia(){
        return view('pages.garantiabmv');
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
