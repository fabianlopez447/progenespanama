<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;

class bmvController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $product = \App\Bmv::orderBy('created_at','desc')->get();
       
        return view('admin.bmv.index', compact ('product'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.bmv.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->file('imagen');
        $Imagen = 'image_'.time().'.'.$file->getClientOriginalExtension();
        $file->move(public_path().'/bmv', $Imagen);

        \App\Bmv::create([
            'nombre'            => $request['nombre'],
            'imagen'            => $Imagen,
            'descripcion'       => $request['descripcion'],
            'precio'       => $request['precio'],
        ]);
		
        Session::flash('message','El producto se ha sido publicado correctamente');
        return redirect('admin/bmv');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = \App\Bmv::find($id);
        return view ('admin.bmv.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $producto = \App\Bmv::find($id);
        if (!empty($request['imagen'])) {
            $file = $request->file('imagen');
            $Imagen = 'image_'.time().'.'.$file->getClientOriginalExtension();
            $file->move(public_path().'/bmv', $Imagen);

            
            $producto->fill([
                'nombre'            => $request['nombre'],
                  'imagen'            => $Imagen,
                  'descripcion'       => $request['descripcion'],
                  'precio'       => $request['precio'],
                  ])->save();
        }else{
            $producto->fill([
                  'nombre'            => $request['nombre'],
                  'descripcion'       => $request['descripcion'],
                  'precio'       => $request['precio'],
            ])->save();
        }
        
        Session::flash('message','El producto ha sido actualizado correctamente');
        return redirect('admin/bmv');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         \App\Bmv::destroy($id);
        Session::flash('destroy', 'El producto ha sido retirado exitosamente');
        return redirect('admin/bmv');
    }
}
