<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class activarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $existe = false;
        $desc = \App\Descuento::where('carnet_codigo', $request['codigo'])->get();
		foreach($desc as $Desc){
			if(($Desc->carnet_codigo == $request['codigo']) && (date('d/m/Y', strtotime($Desc->fecha_vencimiento)) > date('d/m/Y'))){
				$existe = true;
			}
		}
		if(!$existe){
			Session::flash('warning','El codigo que ingresaste es invalido');
			return back();
		}
			return view('pages.activar', compact('desc'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$existe = false;
        $desc = \App\Descuento::where('carnet_codigo', $request['codigo'])->get();
		foreach($desc as $Desc){
			if(($Desc->carnet_codigo == $request['codigo']) && (date('d/m/Y', strtotime($Desc->fecha_vencimiento)) > date('d/m/Y'))){
				$existe = true;
			}
		}
		if(!$existe){
			Session::flash('warning','El codigo que ingresaste es invalido');
			return back();
		}
			return view('pages.activar', compact('desc'));
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $existe = false;
        $desc = \App\Descuento::where('carnet_codigo', $request['codigo'])->get();
		foreach($desc as $Desc){
			if(($Desc->carnet_codigo == $request['codigo']) && (date('d/m/Y', strtotime($Desc->fecha_vencimiento)) > date('d/m/Y'))){
				$existe = true;
			}
		}
		if(!$existe){
			Session::flash('warning','El codigo que ingresaste es invalido');
			return back();
		}
			return view('pages.activar', compact('desc'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
