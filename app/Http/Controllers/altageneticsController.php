<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
class altageneticsController extends Controller
{
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $product = \App\Altagenetics::orderBy('created_at','desc')->get();
       
        return view('admin.altagenetics.index', compact ('product'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $cat = \App\Altacat::get();
         $subcat = \App\Altasubcat::get();
        return view('admin.altagenetics.create', compact('cat','subcat'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $seccion = \App\Altacat::find($request->categoria); 
        $file = $request->file('imagen');
        $Imagen = 'image_'.time().'.'.$file->getClientOriginalExtension();
        $file->move(public_path().'/altagenetics', $Imagen);

        \App\Altagenetics::create([
            'nombre'            => $request['nombre'],
            'categoria'         => $request['categoria'],
            'imagen'            => $Imagen,
            'descripcion'       => $request['descripcion'],
            'codigo'            => $request['codigo'],
            'url'               => $request['url'],
            'seccion'           => $seccion->idcat
        ]);
		
        Session::flash('message','El producto se ha sido publicado correctamente');
        return redirect('admin/altagenetics');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = \App\Altagenetics::find($id);
        $cat = \App\Altacat::get();
         $subcat = \App\Altasubcat::get();
        return view ('admin.altagenetics.edit', compact('product','cat','subcat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $producto = \App\Altagenetics::find($id);
        if (!empty($request['imagen'])) {
            $file = $request->file('imagen');
            $Imagen = 'image_'.time().'.'.$file->getClientOriginalExtension();
            $file->move(public_path().'/altagenetics', $Imagen);

            
            $producto->fill([
                'nombre'            => $request['nombre'],
                'categoria'            => $request['categoria'],
                'imagen'            => $Imagen,
                'descripcion'       => $request['descripcion'],
                'precio'       => $request['precio'],
                'codigo'            => $request['codigo'],
                'url'               => $request['url'],
               
                  ])->save();
        }else{
            $producto->fill([
                  'nombre'            => $request['nombre'],
                  'categoria'            => $request['categoria'],
                  'descripcion'       => $request['descripcion'],
                  'precio'       => $request['precio'],
                  'codigo'            => $request['codigo'],
                  'url'               => $request['url'],
            ])->save();
        }
        
        Session::flash('message','El producto ha sido actualizado correctamente');
        return redirect('admin/altagenetics');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         \App\Altagenetics::destroy($id);
        Session::flash('destroy', 'El producto ha sido retirado exitosamente');
        return redirect('admin/altagenetics');
    }
}
