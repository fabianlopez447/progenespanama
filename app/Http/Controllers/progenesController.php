<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;

class progenesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $product = \App\Progenes::orderBy('created_at','desc')->get();
       
        return view('admin.progenes.index', compact ('product'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.progenes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->file('imagen');
        $Imagen = 'image_'.time().'.'.$file->getClientOriginalExtension();
        $file->move(public_path().'/progenes', $Imagen);

        \App\Progenes::create([
            'nombre'            => $request['nombre'],
            'imagen'            => $Imagen,
            'descripcion'       => $request['descripcion'],
            'precio'       => $request['precio'],
        ]);
		
        Session::flash('message','El producto se ha sido publicado correctamente');
        return redirect('admin/progenes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = \App\Progenes::find($id);
        return view ('admin.progenes.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $producto = \App\Progenes::find($id);
        if (!empty($request['imagen'])) {
            $file = $request->file('imagen');
            $Imagen = 'image_'.time().'.'.$file->getClientOriginalExtension();
            $file->move(public_path().'/progenes', $Imagen);

            
            $producto->fill([
                'nombre'            => $request['nombre'],
                  'imagen'            => $Imagen,
                  'descripcion'       => $request['descripcion'],
                  'precio'       => $request['precio'],
                  ])->save();
        }else{
            $producto->fill([
                  'nombre'            => $request['nombre'],
                  'descripcion'       => $request['descripcion'],
                  'precio'       => $request['precio'],
            ])->save();
        }
        
        Session::flash('message','El producto ha sido actualizado correctamente');
        return redirect('admin/progenes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         \App\Progenes::destroy($id);
        Session::flash('destroy', 'El producto ha sido retirado exitosamente');
        return redirect('admin/progenes');
    }
}
