<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;

class altasubcatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $product = \App\Altasubcat::orderBy('idcat','asc')->get();
      
       
        return view('admin.altasubcat.index', compact ('product'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $cat = \App\Altacat::get();
        return view('admin.altasubcat.create', compact ('cat'));
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       

        \App\Altasubcat::create([
            'nombre'            => $request['nombre'],
            'idcat'       => $request['idcat']
        ]);
		
        Session::flash('message','La categoria se ha añadido correctamente');
        return redirect('admin/altasubcat');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = \App\Altacat::find($id);
        return view ('admin.altasubcat.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $producto = \App\Altasubcat::find($id);
       
            $producto->fill([
                  'nombre'            => $request['nombre'],
                  'descripcion'       => $request['idcat']
            ])->save();
        
        
        Session::flash('message','La categoria se ah actualizado correctamente');
        return redirect('admin/altasubcat');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         \App\Altacat::destroy($id);
        Session::flash('destroy', 'La categoria se ah retirado exitosamente');
        return redirect('admin/altasubcat');
    }
}
