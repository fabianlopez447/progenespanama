<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use File;
use Session;
use App\Http\Requests\DescuentoRequest;

class descuentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $descuento = \App\Descuento::orderBy('created_at', 'desc')->paginate(15);
        return view('admin.descuento.index', compact('descuento'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.descuento.publicar');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DescuentoRequest $request)
    {
        
        $file = $request->file('imagen');
        $Imagen = 'image_'.time().'.'.$file->getClientOriginalExtension();
        $file->move(public_path().'/descuentos', $Imagen);

        $file2 = $request->file('carnet_imagen');
        $Imagen2 = 'image_'.time().'.'.$file2->getClientOriginalExtension();
        $file2->move(public_path().'/carnet', $Imagen2);

        \App\Descuento::create([
            'nombre'            => $request['nombre'],
            'imagen'            => $Imagen,
            'descripcion'       => $request['descripcion'],
            'restriccion'       => $request['restriccion'],
            'empresa'           => $request['empresa'],
            'categoria'         => $request['categoria'],
            'ciudad'            => $request['ciudad'],
            'direccion'         => $request['direccion'],
            'descuento'         => $request['descuento'],
            'red_twitter'       => $request['red_twitter'],
            'red_facebook'      => $request['red_facebook'],
            'red_instagram'     => $request['red_instagram'],
            'empresa_email'     => $request['empresa_email'],
            'coordenadas'       => $request['coordenadas'],
            'carnet_imagen'     => $Imagen2,
            'carnet_codigo'     => $request['carnet_codigo'],
            'video'         => $request['video'],
            'fecha_vencimiento' => $request['fecha_vencimiento']

        ]);
		
		
		
        Session::flash('message','El descuento ha sido publicado correctamente');
        return redirect('admin/descuentos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $descuento = \App\Descuento::where('nombre', 'LIKE','%'.$request['resulset'].'%')->get();
		return view('admin.descuento.result', compact('descuento'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $descuento = \App\Descuento::find($id);
        return view('admin.descuento.editar', compact('descuento'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!empty($request['imagen']) && empty($request['carnet_imagen'])) {
            $file = $request->file('imagen');
            $Imagen = 'image_'.time().'.'.$file->getClientOriginalExtension();
            $file->move(public_path().'/descuentos', $Imagen);

            $descuento = \App\Descuento::find($id);
            $descuento->fill([
                'nombre'            => $request['nombre'],
                'imagen'            => $Imagen,
                'descripcion'       => $request['descripcion'],
                'restriccion'       => $request['restriccion'],
                'empresa'           => $request['empresa'],
                'telefono'          => $request['telefono'],
                'ciudad'            => $request['ciudad'],
                'direccion'         => $request['direccion'],
                'categoria'         => $request['categoria'],
                'descuento'         => $request['descuento'],
                'red_twitter'       => $request['red_twitter'],
                'red_facebook'      => $request['red_facebook'],
                'red_instagram'     => $request['red_instagram'],
                'empresa_email'     => $request['empresa_email'],
                'coordenadas'       => $request['coordenadas'],
                'carnet_codigo'     => $request['carnet_codigo'],
                'video'         => $request['video'],
                'fecha_vencimiento' => $request['fecha_vencimiento']
            ])->save();
        }else if (empty($request['imagen']) && !empty($request['carnet_imagen'])) {
            $file2 = $request->file('carnet_imagen');
            $Imagen2 = 'image_'.time().'.'.$file2->getClientOriginalExtension();
            $file2->move(public_path().'/carnet', $Imagen2);

            $descuento = \App\Descuento::find($id);
            $descuento->fill([
                'nombre'            => $request['nombre'],
                'descripcion'       => $request['descripcion'],
                'restriccion'       => $request['restriccion'],
                'empresa'           => $request['empresa'],
                'telefono'          => $request['telefono'],
                'ciudad'            => $request['ciudad'],
                'direccion'         => $request['direccion'],
                'categoria'         => $request['categoria'],
                'descuento'         => $request['descuento'],
                'red_twitter'       => $request['red_twitter'],
                'red_facebook'      => $request['red_facebook'],
                'red_instagram'     => $request['red_instagram'],
                'empresa_email'     => $request['empresa_email'],
                'coordenadas'       => $request['coordenadas'],
                'carnet_imagen'     => $Imagen2,
                'carnet_codigo'     => $request['carnet_codigo'],
                'video'         => $request['video'],
                'fecha_vencimiento' => $request['fecha_vencimiento']
            ])->save();
        }else if (empty($request['imagen']) && empty($request['carnet_imagen'])) {
            $descuento = \App\Descuento::find($id);
            $descuento->fill($request->all())->save();
        }else if (!empty($request['imagen']) && !empty($request['carnet_imagen'])) {
            $file = $request->file('imagen');
            $Imagen = 'image_'.time().'.'.$file->getClientOriginalExtension();
            $file->move(public_path().'/descuentos', $Imagen);

            $file2 = $request->file('carnet_imagen');
            $Imagen2 = 'image_'.time().'.'.$file2->getClientOriginalExtension();
            $file2->move(public_path().'/carnet', $Imagen2);
            
            $descuento = \App\Descuento::find($id);
            $descuento->fill([
                'nombre'            => $request['nombre'],
                'imagen'            => $Imagen,
                'descripcion'       => $request['descripcion'],
                'restriccion'       => $request['restriccion'],
                'empresa'           => $request['empresa'],
                'telefono'          => $request['telefono'],
                'ciudad'            => $request['ciudad'],
                'direccion'         => $request['direccion'],
                'categoria'         => $request['categoria'],
                'descuento'         => $request['descuento'],
                'red_twitter'       => $request['red_twitter'],
                'red_facebook'      => $request['red_facebook'],
                'red_instagram'     => $request['red_instagram'],
                'empresa_email'     => $request['empresa_email'],
                'coordenadas'       => $request['coordenadas'],
                'carnet_imagen'     => $Imagen2,
                'carnet_codigo'     => $request['carnet_codigo'],
                'video'         	=> $request['video'],
                'fecha_vencimiento' => $request['fecha_vencimiento']
            ])->save();
        }

        
        Session::flash('message','El descuento ha sido actualizado correctamente');
        return redirect('admin/descuentos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Descuento::destroy($id);
        Session::flash('destroy', 'El descuento ha sido retirado exitosamente');
        return redirect('admin/descuentos');
    }
}