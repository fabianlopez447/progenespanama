<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class adminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
		$exclusivo = 0;
		$belleza = 0;
		$entretener = 0;
		$hotel = 0;
		$mascota = 0;
		$nino = 0;
		$restaurante = 0;
		$salud = 0;
		$auto = 0;
		$corp = 0;
		$tienda = 0;
        $desc = \App\Descuento::orderBy('created_at', 'desc')->get();
        $Desc = \App\Descuento::orderBy('created_at', 'desc')->paginate(6);
        $sem = \App\descuento_semanal::orderBy('created_at', 'desc')->paginate(4);
        $men = \App\Descuento_mensual::orderBy('created_at', 'desc')->paginate(2);
		$cate = \App\Suscriptor::orderBy('nombre', 'desc')->get();
		foreach($cate as $Cate){
			if($Cate->favorito == '1'){
				$exclusivo++;
			}else if($Cate->favorito == '2'){
				$belleza++;
			}else if($Cate->favorito == '3'){
				$entretener++;
			}else if($Cate->favorito == '4'){
				$hotel++;
			}else if($Cate->favorito == '5'){
				$mascota++;
			}else if($Cate->favorito == '6'){
				$nino++;
			}else if($Cate->favorito == '7'){
				$restaurante++;
			}else if($Cate->favorito == '8'){
				$salud++;
			}else if($Cate->favorito == '9'){
				$auto++;
			}else if($Cate->favorito == '10'){
				$corp++;
			}else if($Cate->favorito == '11'){
				$tienda++;
			}
				
		}
        // return view('admin.index', compact('desc', 'sem', 'men', 'Desc','exclusivo','belleza','entretener','hotel','mascota','nino','restaurante','salud','auto','corp','tienda'));
        return redirect('admin/progenes');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}