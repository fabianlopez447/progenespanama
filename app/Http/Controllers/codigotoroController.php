<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;

class codigotoroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $product = \App\Codigotoro::orderBy('codigo','asc')->get();
      
       
        return view('admin.codigotoro.index', compact ('product'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.codigotoro.create');
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \App\Codigotoro::create([
            'nombre'            => $request['nombre'],
            'codigo'       => $request['codigo'],
            'url'       => $request['url']
        ]);
		
        Session::flash('message','La categoria se ha añadido correctamente');
        return redirect('admin/codigotoro');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ver(Request $request)
    {
        $codigo = \App\Altagenetics::where('codigo', $request->codigo)->first();
        if(!empty($codigo->url)){
            return redirect($codigo->url);
        }
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = \App\Codigotoro::find($id);
        return view ('admin.codigotoro.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $producto = \App\Codigotoro::find($id);
       
            $producto->fill([
                  'nombre'            => $request['nombre'],
                  'codigo'       => $request['codigo'],
                  'url'       => $request['url']
            ])->save();
        
        
        Session::flash('message','La categoria se ah actualizado correctamente');
        return redirect('admin/codigotoro');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Codigotoro::destroy($id);
        Session::flash('destroy', 'La categoria se ah retirado exitosamente');
        return redirect('admin/codigotoro');
    }
}
