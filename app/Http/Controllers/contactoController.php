<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Mail;
use Session;
use Redirect;

class contactoController extends Controller
{
 
    public function index()
    {
        //
        return view('pages.contacto');
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        \App\Contactados::create($request->all());

         Mail::send('mails.contacto',$request->all(),function($msj){
            $msj->subject('Contacto desde Progenes Panamá');
            //$msj->to('admin@progenespanama.com');
            $msj->to('info.progenes@gmail.com');
        });

        Session::flash('message','Mensaje Enviado correctamente');

        return Redirect::to('/success')->with('success','Mensaje enviado correctamente');
 
    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
