<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class mensualController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function add($id, Request $request){
        $descuento = \App\Descuento::where('id', $id)->get();
        foreach ($descuento as $Descuento) {
            \App\Descuento_mensual::create([
            'nombre'		=> $Descuento->nombre,
            'id_descuento'  => $Descuento->id
        ]);
        }
		$mail = \App\Suscriptor::orderBy('created_at', 'desc')->get();
		foreach ($mail as $Mail){
			$subj = $Mail->email;
			Mail::send('mails.mensual',$request->all(), function($msj) use ($subj){
				$msj->subject('¡No te pierdas nuestros descuentos mensuales!');
				$msj->to($subj);
        	});
		}
		return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $mail = \App\Suscriptor::orderBy('created_at', 'desc')->get();
		foreach ($mail as $Mail){
			$subj = $Mail->email;
			Mail::send('mails.reporte',$request->all(), function($msj) use ($subj){
				$msj->subject('¡No te pierdas nuestros descuentos mensuales!');
				$msj->to($subj);
        	});
		}
		return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
