<?php

Route::get('/',[
  'uses' => 'productosController@index', 
  'as' => '/'
]);

Route::get('alta-genetics',[
  'uses' => 'productosController@alta', 
  'as' => 'AltaGenetics'
]);
Route::get('alta-genetics/{id}/{nombre}',[
  'uses' => 'productosController@seccion', 
  'as' => 'alta.seccion'
]);

Route::resource('mail','mailController');
	

Route::get('marketing-magic',[
  'uses' => 'productosController@marketing', 
  'as' => 'marketing-magic'
  ]);

Route::get('progenescatalogo',[
  'uses' => 'productosController@progenescatalogo', 
  'as' => 'progenescatalogo'
  ]);

Route::get('bmvcatalogo',[
  'uses' => 'productosController@bmvcatalogo', 
  'as' => 'bmvcatalogo'
  ]);

Route::get('celotor',[
  'uses' => 'productosController@celotor', 
  'as' => 'celotor'
  ]);
Route::get('detalles',[
  'uses' => 'productosController@detalles', 
  'as' => 'detalles'
  ]);

Route::get('producto/{id}/progenes',[
  'uses' => 'productosController@detallesProgenes', 
  'as' => 'detallesProgenes'
]);
Route::get('producto/{id}/altagenetics',[
  'uses' => 'productosController@detallesAlta', 
  'as' => 'detallesAlta'
]);
Route::get('producto/{id}/bmv',[
  'uses' => 'productosController@detallesBmv', 
  'as' => 'detallesBmv'
]);

Route::get('producto/{id}/listado',[
  'uses' => 'productosController@listado', 
  'as' => 'descuento.listado'
  ]);

Route::get('politicas-de-privacidad',[
  'uses' => 'productosController@politicas', 
  'as' => 'politicas'
  ]);

Route::get('terms',[
  'uses' => 'productosController@terminos', 
  'as' => 'terminos'
  ]);
Route::get('conocer-tu-marca',[
  'uses' => 'productosController@tumarca', 
  'as' => 'conocer-tu-marca'
  ]);
Route::get('reportar-problema',[
  'uses' => 'productosController@reportar', 
  'as' => 'reportar-problema'
  ]);
Route::get('buzon-de-sugenrecias',[
  'uses' => 'productosController@sugerencias', 
  'as' => 'buzon-de-sugenrecias'
  ]);
Route::get('asistencia-tecnica',[
  'uses' => 'productosController@asistencia', 
  'as' => 'asistencia-tecnica'
  ]);
Route::get('garantia-bmv-technology',[
  'uses' => 'productosController@garantia', 
  'as' => 'garantia-bmv-technology'
  ]);

Route::resource('activacion', 'activarController');
Route::resource('suscribir'	, 'suscribirController');
Route::resource('result'	, 'resultController');
Route::resource('log', 'logController');
Route::get('salir',[
  		'uses' => 'logController@logout',
  		'as'   => 'salir'
  	]);

Route::resource('registrar', 'userController');
Route::resource('contacto', 'contactoController');
Route::resource('success', 'successController');

Route::post('checking',[
      'uses' => 'codigotoroController@ver',
      'as'   => 'codigotoro.ver'
    ]);
Route::group(['prefix' => 'admin','middleware' => 'auth'], function(){
	
	Route::get('/',[
  		'uses' => 'adminController@index',
  		'as'   => '/'
  	]);
	Route::resource('inicio', 'adminController');
  	Route::resource('mensual', 'mensualController');
	Route::resource('carrusel', 'sliderController');
  Route::get('carrusel/destroy/{id}', [
    'uses' => 'sliderController@destroy',
    'as'  => 'admin.carrusel.destroy'
  ]);
   
   Route::resource('progenes', 'progenesController');
   Route::get('progenes/{id}/destroy',[
  		'uses' => 'progenesController@destroy',
  		'as'   => 'admin.progenes.destroy'
  	]);
    Route::resource('altagenetics', 'altageneticsController');
    Route::get('altagenetics/{id}/destroy',[
  		'uses' => 'altageneticsController@destroy',
  		'as'   => 'admin.altagenetics.destroy'
  	 ]);
   
    Route::resource('bmv', 'bmvController');
    Route::get('bmv/{id}/destroy',[
  		'uses' => 'bmvController@destroy',
  		'as'   => 'admin.bmv.destroy'
  	]);

    Route::resource('altacat', 'altacatController');
    Route::get('altacat/{id}/destroy',[
  		'uses' => 'altacatController@destroy',
  		'as'   => 'admin.altacat.destroy'
  	]);
   
    Route::resource('codigotoro', 'codigotoroController');
    Route::get('codigotoro/{id}/destroy',[
  		'uses' => 'codigotoroController@destroy',
  		'as'   => 'admin.codigotoro.destroy'
  	]);
    
   
    Route::resource('altasubcat', 'altasubcatController');
    Route::get('altasubcat/{id}/destroy',[
  		'uses' => 'altasubcatController@destroy',
  		'as'   => 'admin.altasubcat.destroy'
  	]);
   

	Route::resource('descuentos','descuentoController');
	Route::get('descuentos/{id}/destroy',[
  		'uses' => 'descuentoController@destroy',
  		'as'   => 'admin.descuentos.destroy'
  	]);
	Route::resource('suscriptor','suscriptorController');
	Route::get('suscriptor/{id}/destroy',[
  		'uses' => 'suscriptorController@destroy',
  		'as'   => 'admin.suscriptor.destroy'
  	]);
  	Route::get('semanal/{id}/add',[
      'uses' => 'semanalController@add',
      'as'   => 'admin.semanal.add'
    ]);
	Route::get('semanal/correo',[
      'uses' => 'semanalController@correo',
      'as'   => 'admin.semanal.correo'
    ]);
	
	
  	Route::get('mensual/{id}/add',[
      'uses' => 'mensualController@add',
      'as'   => 'admin.mensual.add'
    ]);
	Route::get('mensual/correo',[
      'uses' => 'mensualController@correo',
      'as'   => 'admin.mensual.correo'
    ]);
});
