<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DescuentoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'min:4|max:120|required',
					  'restriccion' => 'required',
					  'imagen'    => 'required',
					  'descripcion' => 'required',
					  'descuento'   => 'max:4|required',
					  'empresa'     => 'required',
					  'direccion'  => 'required',
					  'carnet_codigo' => 'required|unique:descuento',
					  'carnet_imagen'  => 'required',
					  
        ];
    }
}
