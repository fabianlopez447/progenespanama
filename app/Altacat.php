<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Altacat extends Model
{
    protected $table = 'altacat';
    protected $fillable = ['idcat','nombre'];
}
