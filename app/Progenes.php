<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Progenes extends Model
{
     protected $table = 'progenes';
    protected $fillable = ['nombre','imagen','descripcion','precio'];

}
