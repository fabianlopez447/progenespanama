<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Altasubcat extends Model
{
    protected $table = 'altasubcat';
    protected $fillable = ['idcat','nombre'];
}
