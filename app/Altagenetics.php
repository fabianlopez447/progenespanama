<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Altagenetics extends Model
{
    protected $table = 'altagenetics';
    protected $fillable = ['nombre','categoria','imagen','descripcion','precio','seccion','codigo','url'];
}
