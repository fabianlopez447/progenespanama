<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class descuento_semanal extends Model
{
    protected $table = 'descuento_semanal';
    protected $fillable = ['nombre','id_descuento'];
}
