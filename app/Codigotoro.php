<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Codigotoro extends Model
{
    protected $table = 'codigotoro';
    protected $fillable = ['nombre','codigo','url'];
}
