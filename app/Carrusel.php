<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carrusel extends Model
{
    protected $table = 'slider';
    protected $fillable = ['codigo_descuento','imagen'];
}
