<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suscriptor extends Model
{
    protected $table = 'suscriptor';
    protected $fillable = ['nombre','apellido','email','favorito'];
}
