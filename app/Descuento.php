<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Descuento extends Model
{
    protected $table = 'descuento';
    protected $fillable = ['nombre','imagen','descripcion','restriccion','empresa','telefono','direccion','ciudad','video','categoria','descuento','red_twitter','red_facebook','red_instagram','empresa_email','coordenadas','carnet_imagen','carnet_codigo','fecha_vencimiento'];

    public function setPathAttribute($imagen){
        $this->attribute[$imgen] = Carbon::now()->second.$imagen->getClientOriginalName();
        $name = Carbon::now()->second.$imagen->getClientOriginalName();
        \Storage::disk('local')->put($name, \File::get($imagen));
    }
}
